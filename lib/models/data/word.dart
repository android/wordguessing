class Word {
  final String key;
  final String text;
  final List<String> images;

  const Word({
    required this.key,
    required this.text,
    required this.images,
  });

  factory Word.createEmpty() {
    return const Word(
      key: '',
      text: '',
      images: [],
    );
  }

  @override
  String toString() {
    return '$Word(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'key': key,
      'text': text,
      'images': images,
    };
  }
}
