import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/config/application_config.dart';
import 'package:wordguessing/data/fetch_data_helper.dart';
import 'package:wordguessing/models/data/word.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.word,
    required this.otherWords,
    required this.images,

    // Game data
    this.recentWordsKeys = const [],
    this.questionsCount = 0,
    this.goodAnswers = 0,
    this.wrongAnswers = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  Word word;
  List<Word> otherWords;
  List<Word> images;

  // Game data
  List<String> recentWordsKeys;
  int questionsCount;
  int goodAnswers;
  int wrongAnswers;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      word: Word.createEmpty(),
      otherWords: [],
      images: [],
      // Game data
      recentWordsKeys: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      word: Word.createEmpty(),
      otherWords: [],
      images: [],
      // Game data
      recentWordsKeys: [],
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon => isRunning && isStarted && isFinished;

  void updateWord(Word newWord) {
    word = newWord;
    if (newWord.key != '') {
      recentWordsKeys.insert(0, newWord.key);
      recentWordsKeys = recentWordsKeys.take(ApplicationConfig.recentWordsCount).toList();
    }
  }

  void pickNewWord() {
    switch (activitySettings.get(ApplicationConfig.parameterCodeGameType)) {
      case ApplicationConfig.gameTypeValuePickImage:
        pickDataForGamePickImage();
        break;
      case ApplicationConfig.gameTypeValuePickWord:
        pickDataForGamePickWord();
        break;
      default:
    }

    questionsCount++;
  }

  bool isRecentlyPicked(String key) {
    return recentWordsKeys.contains(key);
  }

  void pickDataForGamePickImage() {
    Word word;

    int attempts = 0;
    do {
      final List<Word> words = FetchDataHelper().getWords(
        lang: activitySettings.get(ApplicationConfig.parameterCodeLangValue),
        count: ApplicationConfig.itemsCount,
      );

      word = words.take(1).toList()[0];

      if ((words.length == ApplicationConfig.itemsCount) && !isRecentlyPicked(word.key)) {
        updateWord(word);

        final List<Word> pickedImages = [];
        for (var element in words) {
          pickedImages.add(element);
        }

        images = pickedImages;
      }

      attempts++;
    } while (word != word && attempts < 10);
  }

  void pickDataForGamePickWord() {
    Word word;

    int attempts = 0;
    do {
      final List<Word> words = FetchDataHelper().getWords(
        lang: activitySettings.get(ApplicationConfig.parameterCodeLangValue),
        count: ApplicationConfig.itemsCount,
      );

      word = words.take(1).toList()[0];

      if ((words.length >= ApplicationConfig.itemsCount) && !isRecentlyPicked(word.key)) {
        updateWord(word);
        otherWords = words.skip(1).toList();
        images = [word];
      }

      attempts++;
    } while (word != word && attempts < 10);
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    word: $word');
    printlog('    otherWords: $otherWords');
    printlog('    images: $images');
    printlog('  Game data');
    printlog('    recentWordsKeys: $recentWordsKeys');
    printlog('    questionsCount: $questionsCount');
    printlog('    goodAnswers: $goodAnswers');
    printlog('    wrongAnswers: $wrongAnswers');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'word': word.toJson(),
      'otherWords': otherWords,
      'images': images,
      // Game data
      'recentWordsKeys': recentWordsKeys,
      'questionsCount': questionsCount,
      'goodAnswers': goodAnswers,
      'wrongAnswers': wrongAnswers,
    };
  }
}
