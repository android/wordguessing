class GameData {
  static const Map<String, dynamic> data = {
    "words": {
      "ef18feeb305d01a2427e29785ab05050": {
        "fr": "ABEILLE",
        "en": "BEE",
        "images": [
          "11cde553b2c18da387913da0c68469f1.png",
          "2655de5d9b4dc8d48a3744b7de762f2c.png",
          "2c0d12d24ac05d4b67894d73beb339e2.png",
          "8c85329fe239c75f1a48587696712002.png",
          "96b0f9d48198b25a89e72feae005258a.png",
          "cc1b4f0a43e9c84c8e81883cdb6f14f7.png"
        ]
      },
      "2f751fde9e3003bfafba2814a439fa0e": {
        "fr": "AIGUILLE",
        "en": "NEEDLE",
        "images": [
          "54e9fb141237e71904a6f1fec8415f6b.png",
          "ab1d3ca23763e85f892e25499fea0eee.png",
          "b488273bfe0163cf67edd9943c0a324e.png",
          "ee0b729e88eb297230c4375730553c36.png",
          "f1326722b547e3adafb9744a356500c8.png"
        ]
      },
      "459a48bd35fcd4e0de4897a8cda8084f": {
        "fr": "ALLUMETTE",
        "en": "MATCH",
        "images": [
          "18652a679d2454729399003876a34c53.png",
          "2aa616b5b4d4cdaee88bbcd62af2faf2.png",
          "3bdd6c3bfc8aad616a58a031721cb361.png",
          "6e058c0e1e5f7161219657b1d53d7173.png",
          "714d1dfe5bd4e8deb3dcbbde87c82502.png",
          "7539cb9f143cdd831653227db13a608f.png"
        ]
      },
      "ca2399e21f8f790fd8271b587571ca7f": {
        "fr": "AMBULANCE",
        "en": "AMBULANCE",
        "images": [
          "52a08ad3e62e5969b5003be326546d17.png",
          "5ee84f66780b3535ce9f8dd7a4c49ac9.png",
          "9ac6669b26b7968853bfe87f0d773b05.png",
          "a3d3ebe5fac1d641169c43272f85c600.png",
          "dda1d0e79f892a3da91928267734d3e4.png"
        ]
      },
      "ebdb08c4658053b93419f4b20550c7fb": {
        "fr": "AMPOULE",
        "en": "BULB",
        "images": [
          "4b2b2a6526fd7cac5ea4802aea265c2b.png",
          "8a818e6c93617503a9852bd55c3495f5.png",
          "902d1af26892746754cc6abfad6295e0.png",
          "903f46d7a9d82d3aa3a907487390f702.png",
          "a0d236ef17fffb4f1b17ef2aabd71f4f.png",
          "ef88db9c4d8dfa2b79dd5fcf56d7e62f.png"
        ]
      },
      "9f2f2352ed78fee76d6b12a5ed2d080c": {
        "fr": "ANANAS",
        "en": "PINEAPPLE",
        "images": [
          "0bd746dd5b1eced1653ea515558b794e.png",
          "2e0615b4d1120da9e91c5bbf72475852.png",
          "4c7a5d642beb5ca45fd16d1d1c894deb.png",
          "9a27dcbe9bde235d488ce32623f7ea6d.png",
          "d52edf2693cb289bd28dfd92c85e569e.png",
          "df34ca0eb524505234c036bedebc0b86.png"
        ]
      },
      "cdadb1d96ffe85be400f77684664dddb": {
        "fr": "AQUARIUM",
        "en": "AQUARIUM",
        "images": [
          "2aaaebc668605458a3bde65c8f62c630.png",
          "422f2fd0c9e7bf34a340bee5550e45b6.png",
          "8772001fb6604e8d9a19b53075501cc5.png",
          "a5b081026b5cf09870bd8433b1831d9b.png",
          "b05b7f82f56d9a8df4599d9dfd11390d.png",
          "fe17c0d40a3364898290a5139f55f8b3.png"
        ]
      },
      "1ed3841c7065c0ee04fb63848dcf79f0": {
        "fr": "ARAIGNÉE",
        "en": "SPIDER",
        "images": [
          "02a5e30d43fb06d991bd35af5418ed5e.png",
          "4b282c8d70e3a682ace65f9ff284a827.png",
          "5eaf3d5ba097b16187db016c5fe515cb.png",
          "7e6261b1f870348fe93651d49f64be81.png",
          "8b1b40c2c69bf02016a86fded64e43fb.png",
          "9c9bfe120d39486300c04e02cd85928c.png"
        ]
      },
      "59ff06c55006e8d943f7381f0ce46aa6": {
        "fr": "ARBRE",
        "en": "TREE",
        "images": [
          "30cc36f5f157effa22c7998877ee08ff.png",
          "6d81bfa64fd523e3df0331f64d93a51b.png",
          "6ea407c125a2cc4b69abaaff8a9ae068.png",
          "83f3c2eb1ffe3f4ce82f573dc7073dfd.png",
          "b8ddd03519b3c282d9bc07bd84ef7aea.png",
          "bdbc60323f66fcedf1fbdea761abfa0c.png"
        ]
      },
      "ffa287efd3281d561b2538363a66e5ee": {
        "fr": "ARMOIRE",
        "en": "CABINET",
        "images": [
          "024c3f2a6c844ff206f933c1305f3d6c.png",
          "2c2655150fa67fff0e49a6cff667a16e.png",
          "9a8394e853d9ef697fe0da0eb21b1d3d.png",
          "cdece3f2cbe8558885af332601e6ac91.png",
          "d727a2a4ba42cf81290af101c9f5ab66.png",
          "de1986a24a98ca4d851bc1ea86c15a80.png"
        ]
      },
      "043f365591f39dd53ba63142ea85ee8f": {
        "fr": "ARROSOIR",
        "en": "WATERING CAN",
        "images": [
          "145dcb0305ccaa71f1883c99877ba5a9.png",
          "2f23d9ebc7afc6c8e46ec9631f385b79.png",
          "a2402aabe83b92c87101c303afc6d9fd.png",
          "b0a09d211bd6aa326b1dc4fbb392465a.png",
          "f3d2b8b68690270fc3335e1b7b16756b.png",
          "fa2e583e9a6552aac8e8677e845fef37.png"
        ]
      },
      "5ca359179c7d4dbcc545752eb381f01b": {
        "fr": "ASPERGE",
        "en": "ASPARGUS",
        "images": [
          "06f5c04006f59ba53c0b587c662d7a1c.png",
          "4f75aff24a95b9f0e8799075ec5308af.png",
          "c1239971da75848d09f4a8d6e5fe781f.png",
          "c6651aae72b54b998b094ca264fd0fcc.png",
          "e8c09347016f2f4c83f34ea88cd6eb4b.png",
          "e99f6369973357278a5cfb59f491615f.png"
        ]
      },
      "b0ba1cd3fda6adffba698942753d3e03": {
        "fr": "ASPIRATEUR",
        "en": "VACUUM CLEANER",
        "images": [
          "0a11f6218a9f3745a7dafaacc60f49cd.png",
          "77215a363675cc1fb795bc256a97792c.png",
          "a3be9200f341d62b551152498b6516ac.png",
          "c93fbabcc5f492b312aeb642833510ad.png",
          "cca9ef28460e5f69409b044bb128e101.png",
          "e59dfc8ccc1646b05d779d88e784d07f.png"
        ]
      },
      "58db6f9d8c212cb1928684374571fd75": {
        "fr": "AUBERGINE",
        "en": "EGGPLANT",
        "images": [
          "00790d789a6c0885531e3d62c7bfb931.png",
          "359b2913e7b38134d9f6535fed4623da.png",
          "3d7383e54e2fde3ce4f569eef687efdf.png",
          "ec2c4e91e3100fc86d9c535be98e9764.png",
          "ec67668bac7dab745dbf7ad9a484f02b.png",
          "f86e178919d6077bf0ca2a9ec0d13e7c.png"
        ]
      },
      "9d8da7408d84f1cedaa6dde4eb8b9dbd": {
        "fr": "AUTRUCHE",
        "en": "OSTRICH",
        "images": [
          "2abe3abcf42ae1ad047d7893a0898977.png",
          "63a8100924094aa7729c141fabf2be19.png",
          "739b485cc0ba6f91dbf025caf3995eb3.png",
          "87efc1c69125d04ef5944531a9fab3f5.png",
          "9160aff5d0338b1ba8cea803d77eef57.png",
          "e9aa5181acab8649e82788957ece76e5.png"
        ]
      },
      "c6b3e2e6ed63024622fe9b99742d3391": {
        "fr": "AVION",
        "en": "AIRPLANE",
        "images": [
          "02882ab965c01fc5572d16b291264b70.png",
          "3ed5d5e0dbb04fa9b81393d749f91c97.png",
          "4a16c34208fe1684df850141e67099ee.png",
          "58db49cca4a46b56e52eb2d618c73c6b.png",
          "6964853997c4bbe68904b28b88a8f120.png",
          "94a10fd10b2ea31a8239bad82b675261.png"
        ]
      },
      "674f81cb8ec91b75d6eafeb0c7c148fb": {
        "fr": "BALAI",
        "en": "BROOM",
        "images": [
          "38f93f2b0800a7b7ad6b9629d412c34e.png",
          "5ea4ebf568ea56647fbb9f4d0bcf975b.png",
          "7df08125b737fea5c5aaaeaa5dd38e3e.png",
          "b4df1077750965435d0c998b70f6e92a.png",
          "c1b37c8320699d38d7b9213a1877096a.png",
          "da289d729af4c66d5e782aa29bffacbf.png"
        ]
      },
      "2122a9afa62eb1561b88d4fa15e1cf0e": {
        "fr": "BALANÇOIRE",
        "en": "SWING",
        "images": [
          "22139436d3eecfd8718224f95f63aff7.png",
          "6b4b7534b0ffa670eaef12b0781948a6.png",
          "97f09251445302924a8253e845e606e5.png",
          "9b9bed7e4d6c34acef68b51cacf15dc7.png",
          "9f6d9cbe5173eb503405d1a7458ab358.png",
          "c3af3a06a5e69c15cd037c0887b1d1df.png"
        ]
      },
      "8c5c7974863c13a1b98fb72fcf962e17": {
        "fr": "BALEINE",
        "en": "WHALE",
        "images": [
          "114981bb9fe81bf172c14e4facee2c49.png",
          "160e80ea10a3393ab83e2a933c0fc295.png",
          "4856a69d10a2fa6b18f4e4b1226aa167.png",
          "84bdc89ba7b9a56863495a8d0b547b08.png",
          "efe9420733a75c57d0a870e1316153c3.png",
          "fe45d37efd9808c584208f303d5cba67.png"
        ]
      },
      "1e3b8f8148db2990ad371199c0a1fa01": {
        "fr": "BALLON",
        "en": "BALLOON",
        "images": [
          "17a0d4e7462d6b637f78578516ac097d.png",
          "30d8905926afff932b7738e024a3ceb3.png",
          "5ec4e8832e984023239586a9c0c90961.png",
          "620681ba050335b3dd1106e79f99cb40.png",
          "8a055b8bc1ea8807b930ea04c1048215.png",
          "a3fb5007c4b419d3951256853851db71.png"
        ]
      },
      "06a6eeb21122de15397ccb5830c2396c": {
        "fr": "BANANE",
        "en": "BANANA",
        "images": [
          "39f73cca5b39543d225f878e9727e098.png",
          "48557fa94918f867c2b48f0b4db9bd13.png",
          "598a72ee0a6b07ad5fc7c931eeacaab4.png",
          "7a3f2550183f97be42d02cabfc148a6f.png",
          "7b17a71074e9cd8eb5ef8a2d2bc99738.png",
          "83f6dc03edb71971181a58e4bb3c4af1.png"
        ]
      },
      "2ee183b597b89a271e30e3a661579d21": {
        "fr": "BASSINE",
        "en": "BASIN",
        "images": [
          "2ef6a66c7d267f529bbb9b78cfdd46ea.png",
          "3618c836c2e34aa7e5df577369acfcaa.png",
          "aa68dfc469c4180f5583884ce6313acf.png",
          "c326cdc5cbb821614f17e173cef98240.png",
          "c52eb3db2f253912b80bf91d19d5f8ef.png",
          "fa91371b9b6004c13a8f0d78dff7a630.png"
        ]
      },
      "97e28aed4bfa51eeae2bd8a0970c2822": {
        "fr": "BATEAU",
        "en": "BOAT",
        "images": [
          "79697376634397e2ca718f14b8bae8f6.png",
          "816e739fad1a5672b87dc0b1113b6235.png",
          "b1f3fbb916b1595d24d9d69d385cfed1.png",
          "b562861b0c8d61a9f7c81615a248b1f2.png",
          "c841ebb81c01c63be971376cfc12be62.png",
          "f658ccdae2fd30911f310fa277ebb46a.png"
        ]
      },
      "18839642338add2e034a1d13b054dd55": {
        "fr": "BAVOIR",
        "en": "BIB",
        "images": [
          "68e24d98fd24693295e85345fd7589c5.png",
          "6eb821d9e4ce5f7347008b9907fa4ef1.png",
          "6ee8fc275ffa2a114c5366eb9a9cc761.png",
          "74a48aed4bba12a5b1e849ed4da12a2c.png",
          "b26fe6bce5843a4085f01dde55a60d9c.png",
          "d4c0b1b24284a78d1eccb9ccc8917917.png"
        ]
      },
      "d91e051633772094fb231969235eb6c9": {
        "fr": "BÉBÉ",
        "en": "BABY",
        "images": [
          "17c3c544858e3fccf6f0010edbd1c3d7.png",
          "317a34c4a909d90ace1d8bd035e96146.png",
          "3b7df888f1d3e33db49da16aea659563.png",
          "716d7ce8ffe90e8f0694c6022ed49490.png",
          "d7dd6595383cc6cb73f13625e9a07b6f.png",
          "e4804e18f9034a0aeb7cce038b55d2bb.png"
        ]
      },
      "4fc98c4de3252c33496adb6fbeea3d6b": {
        "fr": "BERCEAU",
        "en": "CRADLE",
        "images": [
          "01a1bda4e1f2b33abf609f10b49e7d96.png",
          "0f74f57bfba511d4e8d0b485ea4442ff.png",
          "4adec9207fc0d6c2e9aca49facde7f80.png",
          "62a4bdb02858a9342ffec1459bc05405.png",
          "9649d56d2faeece799cf33383ecb132f.png",
          "98b4deb52703c34a9c5a3ddc7a43dd14.png"
        ]
      },
      "f99c6c74d323c31482673c433980561e": {
        "fr": "BEURRE",
        "en": "BUTTER",
        "images": [
          "1884d97ad7c3f91da44a6bb0a1824108.png",
          "2f888493de68dd5e7deb12acafab5ad9.png",
          "35e2717eb8fb0eb41de436a39716ea1f.png",
          "505a70eac2c71dec92a281ce49d53f36.png",
          "56b2548176bda6d6d6ecc01a24aa3fda.png",
          "cb1d1aefe69122688a1d630a21cd8563.png"
        ]
      },
      "38a5cfe9af1db218ec2720eeef000ebf": {
        "fr": "BIBERON",
        "en": "BABY BOTTLE",
        "images": [
          "39a727e75a4dd21f4cd0d14cfad2cfa9.png",
          "3dece8aa10916d721d43aedbeede6a73.png",
          "6c5e8f564aac6c9cfc9f61758fa73975.png",
          "70de22121a438f4fbf0c7c523812af2b.png",
          "be66e7fe2dda86710a46b418306cb693.png",
          "cd6dd4827ba89986c0673dd695ca4138.png"
        ]
      },
      "569a110f93fb6990399e4bc6a78ab383": {
        "fr": "BISCUIT",
        "en": "COOKIE",
        "images": [
          "2e86d457620df5b7991c1a3d6d970442.png",
          "3aeab4c2d628ee43efb73531089141e9.png",
          "6bde527c9a69e5e92256c3d9d58817b4.png",
          "99bad980fa444baeaf2763815c26b594.png",
          "9bc59df63c9f649e2ca3e6253f7c6198.png",
          "ba52922c8287766b3009dc8278947881.png"
        ]
      },
      "643f574d34e0828fe85716971d0a5b94": {
        "fr": "BONBON",
        "en": "CANDY",
        "images": [
          "0ee4af4909f26fbc7117a3480f0e96b8.png",
          "0f1d14c68d3cf00756b485d401fbaba2.png",
          "5f42108bcac8c7283dc51d5144350c02.png",
          "85af3559a44c9268d6125b74f9dca142.png",
          "97d373bb76abe9fcd0e638071226ca96.png",
          "c63a19395373d643db8e6fb6973b9d0e.png"
        ]
      },
      "ba248cfda47d466a8b540fc2488e3d77": {
        "fr": "BONNET",
        "en": "CAP",
        "images": [
          "38f725482f81d93745d01e14c8ce8482.png",
          "46dff329ba9c1fc8d61d16c840c4e5b1.png",
          "4ae9728388ae6fc7af3b15c80a4282df.png",
          "7d2de1f3851a2b5f5910ae3a7d315719.png",
          "a76c2d4f58cef225a8931c8910d5149d.png",
          "ee2fa703ae9c1c979747e59e10076fab.png"
        ]
      },
      "70301db0e260e1da8e23ec53ea42aed9": {
        "fr": "BOTTE",
        "en": "BOOT",
        "images": [
          "10de2f6051ac8cc1709b6b009103d867.png",
          "21f1c1f65e6926d7c2bcbe3ef539b84e.png",
          "465f6ffdef7b762b112017c98d5b03b7.png",
          "629df0abbc169017d79f95fe926ed9ab.png",
          "a534b4042a8bd3397a445fb87b92a128.png",
          "f105727d326512f16f3af999339667de.png"
        ]
      },
      "752e644fd91034934c3e7108ec59429e": {
        "fr": "BOUCHON",
        "en": "CORK",
        "images": [
          "6bcd5a5d23a548902d377ce397e70493.png",
          "6dad8720196945e77467a4021facaf81.png",
          "72284a4244a679c8fe0b40f0a72d98fd.png",
          "c98f0c17085d4799cb1fa6819c04ccc2.png",
          "dd02ed03c529abe78bb7b31640982fb6.png"
        ]
      },
      "3ffe940e350b1b45ed44be458f104128": {
        "fr": "BOUGIE",
        "en": "CANDLE",
        "images": [
          "0ebe4fc9601307428bc94485dbe87a2c.png",
          "11ffa799cd382ddc46e311cacae6458e.png",
          "1babc765bd2253facab604eb556f9494.png",
          "1ff5d38a7216309a0f74ffba4423e7d6.png",
          "2a9159a55bd3d31ac926314272852f7f.png",
          "84eee09a972c29a9936eedb2aa39f652.png"
        ]
      },
      "0190593d5cf47da3a5713a089497a146": {
        "fr": "BOUQUET",
        "en": "BUNCH",
        "images": [
          "1bdd95843dad343d02fd75c4004a87e1.png",
          "96805df4fd2c1412f41700f37693f419.png",
          "c823335c9e38384b62ad69c493489ad1.png",
          "f2cc9d451e1d51a44774f18747d68fb6.png",
          "f32847648d078f207564de4cb7eda079.png",
          "fb20ffad76c0b387bc13cfe55924a1be.png"
        ]
      },
      "442704d84afce30c7bed473101c8b37c": {
        "fr": "BOUTEILLE",
        "en": "BOTTLE",
        "images": [
          "3ccd6e0e99e63c6b318f8075cbf9c6a6.png",
          "4162080852f9269af6c0bbd56c83fb51.png",
          "75cfb02d3d97181df9163197638b484e.png",
          "768efdbeafd6d693306bcd72341d92aa.png",
          "844737d9917778d4d651606da3d6bb41.png",
          "84cf800a953ee23ac44c01790c79d8be.png"
        ]
      },
      "76fe94597af511962f0f72081bb4c13f": {
        "fr": "BROSSE",
        "en": "BRUSH",
        "images": [
          "4120a5dcc72fe2e18e2e5ddd115a55ae.png",
          "4667c47eb3bbad2d3cc78abb02c75dc5.png",
          "90c72bffc2f474703b29109b10f59a20.png",
          "9f9cc4db7d37a36cd819be69877d2280.png",
          "a7895739b34650fbabdf85a99263b0ef.png",
          "b917ce5b0af10dc49f9d4635987ab449.png"
        ]
      },
      "ee1755ad19ae037ffa4e256305988b88": {
        "fr": "BROUETTE",
        "en": "WHEELBARROW",
        "images": [
          "08a7cd73f740e525a9325a3262a5d3f2.png",
          "26ce6d73728dcd15d712bfaa03f08dd9.png",
          "2dc0034cb9841ba5db871638c3a3754c.png",
          "3e4c2244a240239f15f80d4425795df1.png",
          "623fe94b638d3f4aeb51c83a3e699429.png",
          "7c13e95aa842433a761a2931050920d2.png"
        ]
      },
      "17932f67884953dcdd7a732b975d5f6c": {
        "fr": "BÛCHE",
        "en": "LOG",
        "images": [
          "138d3130d1e8b55d18d252c1c7c036bc.png",
          "34ce39a7a991b9c1d2f3720d0371202f.png",
          "3a37bdee5c218a583746e84acea130d1.png",
          "716925a99945cbf52632c83daf9b48e2.png",
          "a433a085bfa216725c0663e13f30f09f.png",
          "fb8c468c76c326319054085d2681855d.png"
        ]
      },
      "f3f9602512187b525d6130f6a865adce": {
        "fr": "CACTUS",
        "en": "CACTUS",
        "images": [
          "02bc2fa7230e6ab295bccd344e29eaf5.png",
          "1e61547ed1029676a1ccb9b1a72937ce.png",
          "373e97d2db5d54947eb269979fe470b9.png",
          "8a7a36cc59abbef3f0ce3c633b82bee0.png",
          "a76dd94d66ce7ac711a2ad8ed0d6d3dd.png",
          "c3d782bf9e0cac8b9ca31070c5f1069b.png"
        ]
      },
      "5a5cacece7cdb542d9b426b9c1f3ea05": {
        "fr": "CAFÉ",
        "en": "COFFEE",
        "images": [
          "05e09acd20bedc392f9f5dd2d91d4862.png",
          "981f966696de9b274c4b0210b3346d60.png",
          "a6bd5aa5f84c42f042f9557da81a6b9f.png",
          "bb545c4d4eb3bb2ffcf1889e70f0d84a.png",
          "f48b1e2542b6c3650c1448bf5b79a25f.png",
          "f621fca9297031b3bc050e7f474c0e4c.png"
        ]
      },
      "469f67516caf0adc95da790195648db0": {
        "fr": "CAFETIÈRE",
        "en": "COFFEE MAKER",
        "images": [
          "316c9d94b8169feefe63cb2086582f2e.png",
          "3a2000eb1b87cde8a6853cadb62a5ba3.png",
          "6e9aa136a4a974e800fcfc240b844b26.png",
          "a88d3b9d5a446ed5868358a6a33de66a.png",
          "b2bacc2104291fb6f9664d449b4b4463.png",
          "eb373a05be84ae92e5526dd18a77ffe4.png"
        ]
      },
      "1b489b089dcca2485ad35d4938f2a6df": {
        "fr": "CAHIER",
        "en": "NOTEBOOK",
        "images": [
          "4601f5941a37f24b72b8fd191245040a.png",
          "6b06a8464957249b8554e690542467bc.png",
          "8255035fdd4f113f807b98fd291d57fb.png",
          "957dc7c7395d64f5d59c1dfe38add24e.png",
          "a330c40f30bc9db75fe6b3e83ebf6d01.png",
          "bd8326b928f2a163364e96994d4fc5a9.png"
        ]
      },
      "a0039f10d55f26bbbacda2456bbbf137": {
        "fr": "CAMION",
        "en": "TRUCK",
        "images": [
          "0776f97b5c3e2b107a0af29b05bf4619.png",
          "0c92a8a9ecfd13084a60608a0900ca95.png",
          "46c69b4b287537d66b507b3c8811c24a.png",
          "5a8b843c67cc56db5a1d25b27fa2d8a4.png",
          "64090c9a9f9f208ff7984838c301a2b7.png",
          "d959f83d2bee7a6270b6808723e76683.png"
        ]
      },
      "f2fc57a5a522713009c5f0b8429db614": {
        "fr": "CANARD",
        "en": "DUCK",
        "images": [
          "03e754e252b0e0a6833c7538b9ae088d.png",
          "54bd6d384967d062e997f74b5f425b5b.png",
          "7bf6d8192650bb341be7119d1e735687.png",
          "7ec2fbc33bdcf77056c0edea59bc4c32.png",
          "e6db94ab20a1e95efde3939d1eb8d061.png",
          "ea13af092fbfca7b4396ac296a425ecd.png"
        ]
      },
      "76a3d125f80a61f13444e2ccca8b4df4": {
        "fr": "CAROTTE",
        "en": "CARROT",
        "images": [
          "58b30933cf2bac6a65dbb031fa8aec6c.png",
          "80126a952168043b446d4fccd98aa345.png",
          "912f431901d0890dee6fc338d161a1b3.png",
          "d08dde9aa724cec1935a057c4aef03d4.png",
          "e1ebd3928a6ae63aac3fbb25759ebcde.png",
          "e2577a231c71e600e3ed94accad96b0d.png"
        ]
      },
      "21f18e444506c898eb72129bdd465166": {
        "fr": "CARTABLE",
        "en": "BINDER",
        "images": [
          "05622cf60ab2c28744276db14a10fd54.png",
          "1265fdfcc45c0b8f750ed066e713f1f0.png",
          "32ef43be7447285394c10e2d84e62e11.png",
          "49230b5efd55f554ae18bca5df76d5f5.png",
          "9a978d6f2fbdc0e2e126c0275aeb787f.png",
          "d671062368c0522e4e60e6af7f8f7914.png"
        ]
      },
      "bd5118f7117be09d96991b029d48c159": {
        "fr": "CASQUE",
        "en": "HELMET",
        "images": [
          "282422ef1e0adde8f915c813b70ee51a.png",
          "35a3d95d6c0d7b51a378090a604781e4.png",
          "571ee50f451a134c658a76cecce2ed5a.png",
          "8c2c90eee73546e489e3aaa3050f3832.png",
          "cadaef23b565ff0b2444d6e01138beab.png",
          "f98e8b787287325639119dd404f7f660.png"
        ]
      },
      "67ecc10478c510f60c84b6934495aa01": {
        "fr": "CASQUETTE",
        "en": "CAP",
        "images": [
          "032d8876c5ba2ddd085005348827ff9c.png",
          "503397fe352c4b9ae49e71a5f75e7f85.png",
          "832cd0dacb1a16721e29b543eafdb220.png",
          "8498c6aa957e5d5caaeb64dd0378f530.png",
          "b25dda7b95ce82027daab35b276368d2.png",
          "c7cf34b50a92473527eb249717404d7e.png"
        ]
      },
      "f81f874028a935a23efcd67a8f723162": {
        "fr": "CASSEROLE",
        "en": "PAN",
        "images": [
          "234ef3c370f652d086b346f9705e866a.png",
          "aa39dffa27d682379d487f8913e1b38a.png",
          "cf6b70950275c82f17b4ddccfe3b3160.png",
          "fddcff04b877d151a907a9a734eae4bf.png"
        ]
      },
      "fe9a85979e6bc21893ce2dd9965ae125": {
        "fr": "CASTOR",
        "en": "BEAVER",
        "images": [
          "080cc985578d366ef5c1b903b805d7b3.png",
          "1bdd06bf652fec9aa0879523f8328585.png",
          "3f9854f72f7d351b0c63fc7646c9e0b9.png",
          "9bedf006580326f7f9cc7690329adc63.png",
          "c8efc373f6845921aa1d09ee58f834c6.png",
          "fa4730020e3f3582fd1ec8993dd87bf4.png"
        ]
      },
      "32dcd84cd7484f5decb60fb4af36d652": {
        "fr": "CERF-VOLANT",
        "en": "KITE",
        "images": [
          "03d8992745755d3c590dc76abdbc7c45.png",
          "371267813f78827f54ddab394c8131f5.png",
          "8c1e454de1edd7d997811689322835f4.png",
          "93fd741bfa447a53cbd654413fd46bcb.png",
          "b4db485fb5abbc3bb20337008dcd4799.png",
          "eabb24efd50945b7eab9ef1419dc98ce.png"
        ]
      },
      "3a9ddc995d97085be53d36ff66055e3e": {
        "fr": "CHAISE",
        "en": "CHAIR",
        "images": [
          "3686f868e2611af2708add83be38e027.png",
          "3d130846e8c7dce996800bec2715a4b6.png",
          "602f3118a867432f0b4a981a9db60e1a.png",
          "6c8d7c67433b9ef012505dfa6c9b698c.png",
          "9e956143c43a10ae99dde9c0ae79873b.png",
          "d1ce42e9bd1f6cb1b0a5203c7ed7d30a.png"
        ]
      },
      "443bfc2a91b3811440718a7a9c56125e": {
        "fr": "CHAMPIGNON",
        "en": "MUSHROOM",
        "images": [
          "05ccf6cdcc9e6ad02decbfabcff6a0c6.png",
          "2ebae361147280cd6327190e0168522d.png",
          "3b8c3aa3e0813fa93a1ad6fe50ef298e.png",
          "42d9ed57e545f45ac988f70c36c0e215.png",
          "522df370ae35f6e38b425824ee3a288f.png",
          "53fc504664393059760302c2f8da2580.png"
        ]
      },
      "73c73bd58c121849d2a23fa7dda516fe": {
        "fr": "CHATEAU",
        "en": "CASTLE",
        "images": [
          "33022ca8ff00733a1f262d9f708b222f.png",
          "4f7d0b043e0ac3365734a1795cc7a00a.png",
          "9e7f4c0402f4f0c0da4b756b0822ce2d.png",
          "bb3482fc7b4c0744acd4d8f8fa48dc24.png",
          "bdb8f31c000d0c7e633d1e2171ae7b79.png",
          "c95f478097c79b2bb2065760603a7690.png"
        ]
      },
      "5fab9cc419f528b5b6c3b5fd0b282fd9": {
        "fr": "CHAUSSETTE",
        "en": "SOCK",
        "images": [
          "3b9af08b3102b61d9ca57704ad639b43.png",
          "473596b3221860affbca8de1994a4a56.png",
          "7770dcc5c1fb149432659c5afdba06de.png",
          "cd0239697dc15614b6f83ccab8a3b241.png",
          "dfb468227f14f8cb2683ef7be796d0af.png",
          "ec869bf3cf87c9352e332ebabf9d15e9.png"
        ]
      },
      "3e6bd567ca8376bdf3110137cac2f0aa": {
        "fr": "CHAUSSURE",
        "en": "SHOE",
        "images": [
          "0035503b280a4c704b285650c1701934.png",
          "07d6d66d8e6a75318b80a7b1132a28f8.png",
          "0d02f8c34a634165ec9ee214bcf6d87d.png",
          "0d3ebfe832a292b74932564e80c8890c.png",
          "c90afb24caf15a85e80633fc76ef2f81.png",
          "faa895ef7de9e47541bb45d62b64925d.png"
        ]
      },
      "0846cb12a25da5879aa15e297504a8f4": {
        "fr": "CHEMINÉE",
        "en": "CHIMNEY",
        "images": [
          "016b99e773e39d46be264702e0424974.png",
          "4172669bb923c67fae39e6f42fcc31b9.png",
          "863901ff1e8340a9ce1d9329b017de1c.png",
          "a2ce6e0b9d8b1cdc5f272c2fa6794d86.png",
          "d546122e1fa77f173220d2b2ee910bba.png",
          "eb74f4fbbe95d03344b3b4f5ff84d0b4.png"
        ]
      },
      "7b0c7063130ec693f0500a90bcdea582": {
        "fr": "CHEMISE",
        "en": "SHIRT",
        "images": [
          "61d17d8009d3b3601510c2ebe08244fd.png",
          "6f0e26e3d385311359cdacd36e984e02.png",
          "8d59c491ebf58c76578a49d2c6e00d70.png",
          "ba8c097bf03c860c9c6a4df3719932c6.png",
          "c79c66e3bfb9fd35ce26963a44d5d6bf.png",
          "c8dc3260e103dbac2b16ed77401eee5b.png"
        ]
      },
      "830c2230bfed11f67eaa190376da60b8": {
        "fr": "CHEVAL",
        "en": "HORSE",
        "images": [
          "1969864c431ad200daf373124ae0b10f.png",
          "5d86828ea2ce29481fb3e88d5dbd2571.png",
          "84ccdff791a950f1c979314dc8530d00.png",
          "99851dfed4a7b054d7f2a6dd92211c98.png",
          "bc15dad4e29cf16390216c97c6f8af34.png",
          "dbf1928f5c5a734baf5999551e6b507d.png"
        ]
      },
      "dd85198ce17d96f3ba37bafa2460c468": {
        "fr": "CINTRE",
        "en": "HANGER",
        "images": [
          "05e22fe7bac6e8deafb542541a558d73.png",
          "2fe6708d5a42ab05e7aec633bdd98b84.png",
          "5503caa999e2f0b5f470d776027662f9.png",
          "69ccdf4089ea4ff00789bcf25ded20eb.png",
          "adc0df851c029c7ac157e495a39fde94.png",
          "cfd869c4b9c3626dc66cae0212a275a8.png"
        ]
      },
      "96e09688408fdd3f81a8888a4096ed5f": {
        "fr": "CISEAUX",
        "en": "SCISSORS",
        "images": [
          "394d9f00a3a3fbd638890b6dcd19ec28.png",
          "907dda3c40d7f5ed7daaea2da388787a.png",
          "9ff42b4954921f7920bee6096fa51469.png",
          "a386d6296f64eaffc228ce4180ae2982.png",
          "bbcacc7581bc18aab72b04c000c4ce8d.png",
          "d262d32cf86836764f7125164917703d.png"
        ]
      },
      "001edba4416e6610644f9a82ca7bfa4f": {
        "fr": "CITRON",
        "en": "LEMON",
        "images": [
          "5ec578f32c56058d72084422baf30d74.png",
          "779a3d6dc417944d6427b320bfe07eca.png",
          "a1e8dd26f0b985a0096df034c990d584.png",
          "a6e82bb5a2c494059648970244ee530d.png",
          "d9eabd1b1abaf165aa2cc0e6118a6fdb.png",
          "efda7fcdf875c17657024a58a7413991.png"
        ]
      },
      "61db1d4c8ebe3705c5e88ea540442906": {
        "fr": "CITROUILLE",
        "en": "PUMPKIN",
        "images": [
          "08fee0b75ab9cbbfc718f5156889e1dc.png",
          "5865fc22cf1377e6e7c21a741db36d6e.png",
          "688b0fb41984d54b78c21518e54e0d3d.png",
          "7e44442fbb5c82d7fe462884b8d9ca10.png",
          "c051e0c225d21752a9f460472c20b447.png",
          "d055040f31f75df05af0854e8927d4ee.png"
        ]
      },
      "ffc1e1a2080a82c929c21ed446727edc": {
        "fr": "COCCINELLE",
        "en": "LADYBUG",
        "images": [
          "55e6dfee3cacb5b523c6b44e3e94bf89.png",
          "99db29100815e678ae5314325abb074c.png",
          "a6f8c3a71b9b5859d96733499c270410.png",
          "b66a0e5bda4d721959c5680a298114de.png",
          "cae7522c76a3ec5fc38bea21d7b47b33.png",
          "ed7d38e5acb4250e8c601490b2696f9b.png"
        ]
      },
      "ac57c7697658ce226e6c9df697c6309a": {
        "fr": "COCOTTE-MINUTE",
        "en": "PRESSURE COOKER",
        "images": [
          "103841a9c723c14ba5a6cc972fa86751.png",
          "3cdaf3aade5b9729a0d05f4f31bcc5d0.png",
          "73e58bdaad2367cb1b337e516fab76d1.png",
          "8d69b1d478a8adc684e588ceb09ee499.png",
          "aceeab8db86cfaf5de99aea5014e318e.png",
          "ed3e1e9c24a8c8400ac8604282931afa.png"
        ]
      },
      "3f1ff6aeed1ef2c02a148702362fd8db": {
        "fr": "COLLIER",
        "en": "NECKLACE",
        "images": [
          "052f7dd199fd68544d00a30e66d050c1.png",
          "0b34344c9795a0f9656ce2cde705986e.png",
          "2af7aef767a1382c4ef1652136dcc004.png",
          "4040c32760c5404b4e49a8806871e46a.png",
          "8ce2d6955c9ad2bae689810d8bf92922.png",
          "cf8e51c36c1f17e54e5398a4f45ab498.png"
        ]
      },
      "86725cb078e4a8eaa3bcc8d7be021d05": {
        "fr": "CONCOMBRE",
        "en": "CUCUMBER",
        "images": [
          "307d2e7f1aed5de63c2a0a08d8e707db.png",
          "70c31afad51cbc34b01a7941097b9f48.png",
          "873a41c2ee8ebd452bc9f6d0b97caf98.png",
          "88385f789e6e7fc92db1ef49085e5762.png",
          "96ac2c9fd52be42fc3b5fa9c670a9e7b.png",
          "f8d48ed943cd79593d1be4580e755a7b.png"
        ]
      },
      "27f8c8e369037b981b6e9a11035bfad1": {
        "fr": "CONFITURE",
        "en": "JAM",
        "images": [
          "2309405e723d806638a764359da20d31.png",
          "509e1422b3041719c217e8bca9d1292f.png",
          "854f9dbb85661da88b5f09c9ac58029d.png",
          "a5d574cd6cc78403744ebf83d66db114.png",
          "c7ecfd1679e58eeacdd1fa3fda6db063.png",
          "dfef5429170751a4d795454e2ead0697.png"
        ]
      },
      "31804b3524cbd49cc8c553aefe721225": {
        "fr": "COQ",
        "en": "ROOSTER",
        "images": [
          "08e590c180141cdfca9e664df9d74c63.png",
          "5608e183fbccc0518a21512a0ffd6216.png",
          "8b83fd9f423f6f2e0cf047fe40e6eaf3.png",
          "bf6245897af47fdd1a3646a0993d1562.png",
          "dee1b847c397f9a3ed11467126ce585e.png",
          "fc858996e14e863c76517ddd15b2f9c8.png"
        ]
      },
      "133bb809d2ced51a5bee33051cb90470": {
        "fr": "COQUELICOT",
        "en": "POPPY",
        "images": [
          "3943ad3f58bac3b65846909a61c43638.png",
          "5269729a4dcc6a0022abcd65c5ef1b08.png",
          "984d079380b9d6d9281cbf067ae13251.png",
          "de63ee3f992b80be8d76ca0982dca55c.png",
          "de7e39d0ca8d005c68825ca9fcdc8ee2.png",
          "e21fe71db77f211ae78ed4713eb1d178.png"
        ]
      },
      "b16f795bb364fba5ec3fd0acd06e66fe": {
        "fr": "CORBEAU",
        "en": "CROW",
        "images": [
          "559cd54e944a103843cd5ca387431362.png",
          "81ab43fe1bc55b7b449fd51e0145c0f1.png",
          "87df429ceb3803dbb4cc19da124f0669.png",
          "a1cc580f0ee7780ca905c527e0e13e53.png",
          "d86af41031bbacfffc6484748281f712.png",
          "e6c93d2bd17861f8e0bde193504eb108.png"
        ]
      },
      "354e983251b9f1f3e839e9988786d3cb": {
        "fr": "CORDE",
        "en": "ROPE",
        "images": [
          "14fcc32903365076bf9c37de278d0bdc.png",
          "43a1ac959735c18d95495fa54b38f0ef.png",
          "8ddd41dc104dea343c1f52e5838bb9bd.png",
          "8e1e2fc2a07f3868cdf2de0fca3c797d.png",
          "bb31af732cd0a314bb840daea40f2f00.png",
          "bddefbe40162e2fc6876c679f94b06e9.png"
        ]
      },
      "f01031dfc2511fdb6604da650f1b7f65": {
        "fr": "CORNICHON",
        "en": "PICKLE",
        "images": [
          "0a9c12b9840d90fbe90915b8662e5517.png",
          "2795fb2a11cab7505a6f5d031b302f9b.png",
          "392dcf718ff5ae489b0221dcef10699b.png",
          "56a194d0ea2a21b7f1322aafc1739a55.png",
          "c823a6db8309bb585a4d8da4a1bd73f6.png",
          "ed79a240f33ca3bebd9826230ac09cb7.png"
        ]
      },
      "b0f498c7bda4c468a511ac9beb6d94a4": {
        "fr": "CRABE",
        "en": "CRAB",
        "images": [
          "1e555a63e5d58818ac975534fc118fe6.png",
          "4822329b475c27d6583da1ef71c4b5b3.png",
          "7e863bebb2ae5cbebd105bb80507daf2.png",
          "925b1d99a832231eed28920192491a92.png",
          "a81b1f8cda1a9318276e74e25e0106fb.png",
          "f4d2db70575ade0d9b37530d7725b710.png"
        ]
      },
      "2f1b1f59372966736e31d35dc9749727": {
        "fr": "CRAYON",
        "en": "PENCIL",
        "images": [
          "10d2d4de3ba9a0b914182cf27beace57.png",
          "51204d90b1475ea7c62498b751d7185f.png",
          "58918560295a7415e20640b7572ceb5e.png",
          "7d2ba96b5c3a505fa47a58dc647d913f.png",
          "e44df5c16ab7f761412924cfc4c7ba8e.png",
          "fc4e2f759a50730140a0c89033c71826.png"
        ]
      },
      "44a015e6ed3667695c22ce030b4d2b21": {
        "fr": "CROCODILE",
        "en": "CROCODILE",
        "images": [
          "0c4919191c93f140e120213a4f9c5d6d.png",
          "29c6eda4badb78ce520c9a08395f4e7c.png",
          "5bc94759b9e29e7a34ef800f3ed44052.png",
          "6a34b520351c2609095ce9bb73089059.png",
          "7754a50618734c4824c70817670e200b.png",
          "9c31872987843fa7c409cdb57a033668.png"
        ]
      },
      "7e684540592acef353e5260f3782fa52": {
        "fr": "CULOTTE",
        "en": "PANTIES",
        "images": [
          "297f3959b1af8b589dc74701166a5a03.png",
          "2fdf7fc1fc3c89546fa7094dbbfb1d9a.png",
          "3d83688020bf2a8787386c7f69968854.png",
          "57d533a5d395fdce03c210ff478819f2.png",
          "68ffcc854a6a560e9983e73e9d51264f.png",
          "f04962feffb6fe9f1ce6d29c1216f83e.png"
        ]
      },
      "7ab7618946f29b8a96a017b10cd3a069": {
        "fr": "DAUPHIN",
        "en": "DOLPHIN",
        "images": [
          "3ad392072172b74c01b42b518bfa79b6.png",
          "409b47ff2034adfbce1f22f2aaea2e61.png",
          "46e397c73e38f2d676278d50d688995f.png",
          "5208f3f0c16061b228437f42692defd9.png",
          "6c7dac36ab21a3193e597d5ae5c05dcb.png",
          "7a762f4fc1d5babaee11ae820ab12ec5.png"
        ]
      },
      "8db06eadc925121fe6f588c770ffb231": {
        "fr": "DINOSAURE",
        "en": "DINOSAUR",
        "images": [
          "00572c44c7251775c95bae9d1eb561f4.png",
          "230f35d11d1f9ff580208c6bc0afc55b.png",
          "32f441d725edc2e52535d3778f638842.png",
          "3e5c66a0b5f0e760457664420d699e46.png",
          "9b30beab61f54f43ff86c8989e4776b9.png",
          "eaca0641cf7d3a5e3743354c91eb44be.png"
        ]
      },
      "1a7e329b6e3478396f9788cdd7420835": {
        "fr": "DROMADAIRE",
        "en": "DROMEDARY",
        "images": [
          "39fcdc85b7e9ed8f3ec4d4037f9b075c.png",
          "4c2b4149aab210060f92ef5b9cae480c.png",
          "7e42219ac4fd66a6cc0ef5bc8e1f96bf.png",
          "9cd9bb0ac805c18d008fb2ef2008b5bc.png",
          "dd4772bcb9a15325a3917ba8e4de8e57.png",
          "eef9dad2e18b27cf9ed22e539ad0921a.png"
        ]
      },
      "c847ffbef59fd0195df97a306e263934": {
        "fr": "ÉCHELLE",
        "en": "LADDER",
        "images": [
          "76fc561e3cc8875762b52136228085f7.png",
          "bac5b1f1b993998abbdaec86575281a9.png",
          "d09aafead9632b0e7564e5c9b4848ce5.png",
          "d80fd5de10320581f103f34b2b0ef22d.png",
          "fd316cad7fd1389f0ed37d7f4051e9c5.png"
        ]
      },
      "ba28710763ca0a21ce05d071e5a88146": {
        "fr": "ÉCUREUIL",
        "en": "SQUIRREL",
        "images": [
          "20a363b23fdbb4807deaf37df96f4fe6.png",
          "2ac1a2e3cdba7726f1f14fb435742f13.png",
          "6c0f0d51f3b6198840b579f47d826d31.png",
          "95583e34ea01bc799cd21b7f905d121c.png",
          "c36faeb68baf853f83f7b0cbd667ca71.png",
          "dd5e56c0e56c8d2ba4a10186f577f3e2.png"
        ]
      },
      "2959692c41f22742c554a223ecc5ca55": {
        "fr": "ÉLÉPHANT",
        "en": "ELEPHANT",
        "images": [
          "0c003df0f3a754d4b73a30e47db821f9.png",
          "0df062cea3fb7a02c535e5f21f2830c1.png",
          "84b8de2b978a5e479f4e23010584e7dc.png",
          "9b43e49b0f15884ea030b8c2fc4fc054.png",
          "9f7421aa704b8eb6167475529d50054c.png",
          "e620ec4ef12f8aea19095937f587ad63.png"
        ]
      },
      "3a9206d323bb693685c86460cba955b3": {
        "fr": "ESCALIER",
        "en": "STAIRCASE",
        "images": [
          "18fe8c26f2966a9bd3334a24798059cc.png",
          "4cbf52843d009efd86a0e029a1f39660.png",
          "686ee308096944f4c0eb3ac38ddc4c27.png",
          "9c97720784201728bc9d7e27ebdb5995.png",
          "a98eadcd6e0c80264b3a278bffabc418.png",
          "ffaf5f9d1da63511077946d2639cc900.png"
        ]
      },
      "7582c8797c9351fa02cd863c8ce98bc4": {
        "fr": "ESCARGOT",
        "en": "SNAIL",
        "images": [
          "16f3779940c06d49566e08a613493e27.png",
          "3a0b28fc8c3775b68eec0c04b0a6859c.png",
          "864236a89485ebf6a876be25f65b9ec5.png",
          "8b092b87c4b92a98a96fbd07b18e2a1b.png",
          "8b8f06a2287206754af947924e9583d3.png",
          "8f4828e82b5e11a548deb57fdf98de3c.png"
        ]
      },
      "31dd7bb49723681d7930ce82e4da5dbb": {"fr": "ÉTOILE", "en": "STAR", "images": []},
      "b129a41ba25423464f1b6e93c2cf1769": {
        "fr": "ÉVIER",
        "en": "SINK",
        "images": [
          "0e98a40be27754bd43ac471dd7defe0d.png",
          "19552ffe25b5fe590767f42f58d3d28e.png",
          "1bab9520f8b33550ae4fd385ade8b34c.png",
          "4c8d839365e5c03209d3b2976a629f76.png",
          "6d3c1e3859553da1dc5863142d042b62.png",
          "9834c68881b8a44f24d8e4ed13e6c751.png"
        ]
      },
      "0fb3931d3b48eae9b2e58b31c54839fb": {"fr": "FANTÔME", "en": "GHOST", "images": []},
      "e0d80cdd189bad8426d46b1ef1ac583b": {
        "fr": "FENÊTRE",
        "en": "WINDOW",
        "images": [
          "2c088eb555f2d1b7a6dbc7bfb3f032d8.png",
          "82ae638e12af72d5d377f13e354b305f.png",
          "957be73140d7a0856e302e2541147638.png",
          "bd6d789c6d8015d695153b67506df79e.png",
          "c4e426bec875be6db6632da2548099d1.png",
          "cc04022ac1f2a960b86d36339850d01b.png"
        ]
      },
      "c976c782c1c38a21b022a06baea15f41": {
        "fr": "FLEUR",
        "en": "FLOWER",
        "images": [
          "3a597b3bcfb215c6d51416dd9045babe.png",
          "4950974d34fa2205b984bc6b0abdfbd1.png",
          "56b4c25f54236a6f45d11ec129585b48.png",
          "5d07ebbd5c6cc407a78a66cce0988aeb.png",
          "8328e4d8b92509ff34a381d91598fc48.png",
          "9b4dc5360b1455bebb4a7208156b4d1a.png"
        ]
      },
      "465f875cd914292dfc32d41c0930750a": {
        "fr": "FLÛTE",
        "en": "FLUTE",
        "images": [
          "09adb69efe9891daa9d48ed2223c05e8.png",
          "1c574277fa5550a533aad3a189e57181.png",
          "47335efa64a09db3c4629641d34a9ea1.png",
          "9a675e4a2185e05418729b653ffad6bd.png",
          "e1f2513ca3da869268e3005bc43685fa.png",
          "edd034b176de6a3652cddbb8ac1a1b8b.png"
        ]
      },
      "1905a6ebd79ef3ca99dbca661e8807ab": {
        "fr": "FOURCHETTE",
        "en": "FORK",
        "images": [
          "36ba411859c3e0faf38461af9541f632.png",
          "50817ed0e6dd4dbee6f8391ff9a98d92.png",
          "888d42da77b6ab25247514c4a5e509c2.png",
          "e5b9abbc25618099e5362072a8cf28ad.png",
          "e623e5158945a4a8784b4e5f92fe0bf3.png",
          "fedd3ef25cc8ed85bd708f63d7fc862b.png"
        ]
      },
      "2a72b8b07cae16c4ea0d999fca9a3ded": {
        "fr": "FOURMI",
        "en": "ANT",
        "images": [
          "0bf3a3b2582ae249af783039b933ebc3.png",
          "1a2285de53cd42ec4590131c948733e1.png",
          "1dcdecbec174fdedf03fa100e04f6175.png",
          "384c9c180d339b67bf9e498a81b31173.png",
          "3fa43e20aa457af292f6e7436ad94a09.png",
          "85a4d8c8364cb760cdb1af7bff07228e.png"
        ]
      },
      "dbc194a9c453c3fe4f08269c0d56fa18": {
        "fr": "FRAISE",
        "en": "STRAWBERRY",
        "images": [
          "01a7eae4d29cb655dcb197bf512f3684.png",
          "10e1d5e58ea4492764c666be9973fb99.png",
          "163393d94ad908de5b8adfd7e86020e1.png",
          "61551d786f2951e9429a6b1b869621a1.png",
          "6e0d8db0e54bb929586497cfc221ac17.png",
          "c8c7d4fe49b10320ffba62bf0e263848.png"
        ]
      },
      "658b7cd46fd8d132d704998924386893": {
        "fr": "FRAMBOISE",
        "en": "RASPBERRY",
        "images": [
          "0bd75eed7abf2a8a100cf4d3ca6d5387.png",
          "22eaed0002d2e90d0d41cdf5f4b3382a.png",
          "255062a7d0c9fee54fc62f42688c057b.png",
          "3fc52bda8454faaf5bd069a3e4a2025d.png",
          "67cf67ab977a3f2a4e5785f90d171869.png",
          "ad661bdd679a209834a6ca854b636aca.png"
        ]
      },
      "559e149174b6ac29da0862b1c129ad0d": {
        "fr": "FROMAGE",
        "en": "CHEESE",
        "images": [
          "5234dbd6fa5b2575c5d23ace4f81bfdf.png",
          "8fe2965dc37cddf7f7ac6939fb1a51d1.png",
          "a375e113524d34f047ed98f1d013b8bf.png",
          "b6a004e1dc86b23817c21986c9767dd1.png",
          "c585fde998c18627cc5b60b4523843a3.png",
          "fa9c6c616a4316ad99e8ff2a81f2d955.png"
        ]
      },
      "f655ae8a5bd45e7f9e5176ffb5b10ee2": {
        "fr": "FUSÉE",
        "en": "ROCKET",
        "images": [
          "086d13e213975b8d0346e5fd2c0a4649.png",
          "61ae3500126beb33931103486ceb5f48.png",
          "6a9939a4ed17039417e7be1552b66ddf.png",
          "b226cca6167eeef5d2cd50ae989279cb.png",
          "df7252cc023c1a0ce6266141b6ba4f9e.png",
          "f86015c680ac42302ed71bdcf7039ffd.png"
        ]
      },
      "da1bad3df085cb43989e5cbd9044f7e2": {
        "fr": "GANT",
        "en": "GLOVE",
        "images": [
          "0515ebfcb05958f40f58868f31368534.png",
          "5fabf249d7ede0594faea632991dd300.png",
          "9c66481fa67048cad05cf03e6c9d06d2.png",
          "a4f8f311d5d4d1ea1da183043c29078b.png",
          "b28c5e0c3660ef6e1d4e16da18a75dce.png",
          "d8ea561c65f54f13926328f38e2db2eb.png"
        ]
      },
      "894f4ab8710a7076c96baea2ed83bddd": {
        "fr": "GILET",
        "en": "VEST",
        "images": [
          "31cfda02f0caf1fe90ad34bd5999efcc.png",
          "401b1a879e989a67c2bc3d81d952260f.png",
          "450b75a77e86c0adc74b25f2ed8ea988.png",
          "867de4f116e50470a809039f59960cbc.png",
          "c17edd712aff499c48444cf274d2c62c.png",
          "def03970c473935cc6552d5f1c89fed2.png"
        ]
      },
      "869038ca1cd9992a2452ae6bbff88866": {
        "fr": "GOMME",
        "en": "ERASER",
        "images": [
          "2fbbbb5e35769ddc6d0dd125e1035a29.png",
          "33a7f85db88d86fa5f0fb437f5d970bc.png",
          "5539c8b7d679fe6f90f4ae6db8e0cff5.png",
          "b4979884fdca7e145b5ad0afa4ffb6f1.png",
          "cf30f54d83fb81bd86a7ba33224e4482.png",
          "dd29c72c97da4152d2c50fbcee1d06eb.png"
        ]
      },
      "dbbbefc60ccdcdf4860abd7e4126db81": {
        "fr": "GOURDE",
        "en": "FLASK",
        "images": [
          "2188fcc345df378953b5be98b5353f89.png",
          "8edfd427e11a9bbf2ef7db74fbbb6faf.png",
          "a9f1138ebd09c03d591e4c55c2d27e90.png",
          "d08c22751d34970d7d817a97201434bb.png",
          "d484fc009eb2d2ac91886be296f5852d.png",
          "ebe73ddfe459f8ae32a4dff229e3591f.png"
        ]
      },
      "0421f8e87929a08cf0a48907e65787c7": {
        "fr": "GRENOUILLE",
        "en": "FROG",
        "images": [
          "44b8e76264d8b7500349d6b6307714f6.png",
          "731bf4ef5ba6350bb7eca7d0b400b2be.png",
          "d395583b2392d7e2e1690e9730979396.png",
          "da26ece2a924fae1fd030a1eaddb5ee8.png",
          "db9ab5c7ce8e7bc5b94d9f355df303d5.png",
          "eeaad3346be230f02d991d834d7e7711.png"
        ]
      },
      "ef7fd3ca78900a627079913e5ea796e3": {
        "fr": "GRILLE-PAIN",
        "en": "TOASTER",
        "images": [
          "47bed6b5e691d98cb3f61676faf5ce0b.png",
          "4e9900a002f0af4f075d55d940ce492d.png",
          "6f18ab0d5d8d995b57f4ed7dae83a3d8.png",
          "83bdf746a3653d51f652ae9023b528d9.png",
          "b75e0485c20738a2d896e04ebac3b5d4.png",
          "c454eec0b0e9057d9f0617e84fe382cf.png"
        ]
      },
      "012688788526d1247495c10ca388fbf6": {
        "fr": "GUÊPE",
        "en": "WASP",
        "images": [
          "45162c9f97714fa55dabd4a79d5ee2b9.png",
          "61522daa6f4b56a7aa34d440d4dd33d4.png",
          "75eb36dc0e2141c0aa60eae8a88661f2.png",
          "81dbcb4799497a9404e2bb1d8fd5b738.png",
          "9e314a7e6c9b5203d6edf979abde8ed8.png",
          "b3eb8331454b06fd539c2496c281a77c.png"
        ]
      },
      "b17fc9b059af74db18adad87bf9b7eea": {
        "fr": "GUITARE",
        "en": "GUITAR",
        "images": [
          "2c44ad055c257184672e48dec811be88.png",
          "3198493d2155541ed86e176cdaaa54a0.png",
          "7625f8ebaa586a90ab06c4acb40afd3e.png",
          "7b13ab3adbde26b2f90a2631b6ccea19.png",
          "94ba0774f51ee397912a9ff5063909a5.png",
          "95bcf3f83f0cf00025240ea9298a52b1.png"
        ]
      },
      "debd4f54647ebd79446afc611bac5e13": {
        "fr": "HARMONICA",
        "en": "HARMONICA",
        "images": [
          "13484abcdfa8e8293a8f081738e6fa03.png",
          "1fed9a037f68ed0bc64244be7897306a.png",
          "54849aad345eebacad2c5893328ed2d5.png",
          "9c622c7fa5d0c1abd9dd2ae3482db72b.png",
          "c7db68454a0e30c397eb0ae5c89625c7.png",
          "d931672779a2cf146236c58584db1081.png"
        ]
      },
      "931537a980397b5730a3670dbc28a389": {
        "fr": "HÉLICOPTÈRE",
        "en": "HELICOPTER",
        "images": [
          "363147ba6dcb1c53d8cc27b06c30214a.png",
          "4fcc733c2be90063caa1aa72ac60383d.png",
          "6af5cbf94ada858c630e1449f5aa6354.png",
          "ed7dead124913c6137ebd95e988d47ae.png",
          "efdcb58883b5084a33b9c0f7c378442b.png",
          "f700999f7732a1aa5370f8a0205aabee.png"
        ]
      },
      "116d41d0db4027174b84636ff1b048b2": {
        "fr": "HIPPOCAMPE",
        "en": "SEAHORSE",
        "images": [
          "44ad60a36e7bc2b3c2a8c4c525d444e7.png",
          "698d8ad74d7b1d7b28a4ca8cedb98f6e.png",
          "6ae55c27178cb3e15670ba82094edd21.png",
          "a7d5e393c9e095dda40864abff4727fa.png",
          "b6194194a233ec5a67d961ff68149d30.png",
          "bda8bff7709005be7527a7a44b47eac0.png"
        ]
      },
      "bbfbff785d9f5dadbe9d4f86393cf619": {
        "fr": "HIPPOPOTAME",
        "en": "HIPPOPOTAMUS",
        "images": [
          "0a9ea740abebdf95082eb68b77e7c0d3.png",
          "0ed03645302e9effee99487f9a47dbf2.png",
          "365df08ec1304a0d5e5f1a228d30ffba.png",
          "7d224d3e94cad3a2054c08e10cc77fa6.png",
          "ad8bd4ae35f7ff2129f42a4384754dc5.png",
          "df8ab1714ae241dc5408306216e3bddb.png"
        ]
      },
      "3df2231184ec922569b8ccc1e7799ab8": {
        "fr": "JAMBON",
        "en": "HAM",
        "images": [
          "07fa8204569c265fd1a179e8bf698b20.png",
          "13e2f06000482e9e7e0bb80933e7a41d.png",
          "1db6bd0287d25ff64bd072d4d4826f0d.png",
          "95768e7d24310d2dfa6e78dde005a6d0.png",
          "a93ca0d4fcc36e23104aa0cabd43fd41.png",
          "ecfb82fe6deca08964fa48e187d4202b.png"
        ]
      },
      "415f0e816d8a4bd3a3f6c80730fa66a0": {
        "fr": "JOURNAL",
        "en": "JOURNAL",
        "images": [
          "4e9b2a42b0491fccbe380119331d542f.png",
          "6c56c2f4f418b0c0809c80c234fb2eea.png",
          "88c8bde18ba75406fac979232317198b.png",
          "df63e5123e2f377bb37b0ba60ccd1d0a.png",
          "f0b02fdbb1b0a25db47689c8976861a9.png",
          "f4ad6af2ce56caf2825e1260686ab02a.png"
        ]
      },
      "cee322c154967b2d7f8dfcec9a5f5640": {
        "fr": "JUPE",
        "en": "SKIRT",
        "images": [
          "155acaccfde5bf93b10d9f668cc22f56.png",
          "61ec3b1ce84a2ed0af1a557363823295.png",
          "919876fd4b19b5c525c806c4a5eea869.png",
          "b0b9e60a1b193f638a2b8a2308e31cb4.png",
          "c95eb2240cfdc62e4f9b62681b7d00b2.png",
          "e0e95978de25fab98ad964306ca31465.png"
        ]
      },
      "c0a23af949a850191ab202a3c3be3b84": {
        "fr": "KANGOUROU",
        "en": "KANGAROO",
        "images": [
          "36cdd1190365b19c0393ab99bfa08c57.png",
          "397a78de2d89babd4b898bb5313619f3.png",
          "9a48f2333638bb1e40a7433a3b434382.png",
          "b7805f5e79c35e4290865fc5b5a36340.png",
          "c6bab45b2092af032504cfcff9cd15f4.png",
          "e7df891997b5a06955ff5fa7e4a9df09.png"
        ]
      },
      "3cb0956f788cd607a40e72832dd8b492": {
        "fr": "KAYAK",
        "en": "KAYAK",
        "images": [
          "0def85dd0ebfe973e2b56166aea155b0.png",
          "7751b9bfc7b452fca8f5d9489299b1d6.png",
          "84dbf606effbe50b1b5e3b001a1043e3.png",
          "acc13bc6f84ca2bb4869091fc3e41068.png",
          "e30a8941eed93e08c7e1ee3532c017ec.png",
          "f0411179a31b4a6be9fd8dc7e6eb33ac.png"
        ]
      },
      "decb4bf56fa285db3870c4c64ae07abf": {
        "fr": "KIWI",
        "en": "KIWI",
        "images": [
          "09d5a7c610566d674f35ddf2524ab4d6.png",
          "1052fa8258f32c35519b0b173f559749.png",
          "4c6a9276bb7fd7102b92d5a8378a66a4.png",
          "81d5e843c9a4ac81f793529a0b33cf7c.png",
          "8873b069e005435090730bff1d4f6c75.png",
          "df108fe4b49d031e28867fe1b7bfcf98.png"
        ]
      },
      "b0c387a619c6d9700ea51d832c109027": {
        "fr": "LAIT",
        "en": "MILK",
        "images": [
          "277a36c6d020b0d954e4710bb5080be4.png",
          "2c1729f52db7c685b50335a1fbe2b666.png",
          "4adba45fbdbd49f03f07c653a6026558.png",
          "8160619ec9f656f50b7e3214ac30e8e0.png",
          "e645506a362af82d5244cfba4439f2e2.png",
          "e91d20780ed51d50d5751d0d51bc8786.png"
        ]
      },
      "660ebe84bfae21a67ae36dd69e17fc87": {
        "fr": "LAMPE",
        "en": "LAMP",
        "images": [
          "07636668451ad2087c2ed8669237c508.png",
          "4aba88a38f0762c7fb438db88d47327e.png",
          "4f990667eda58c60fc34a3ec57b7c14f.png",
          "77b8ede28cefcb84593bdfa39d690f48.png",
          "b66fc69f7a579521d7c511e9d45610c1.png",
          "eff65878fbb3f3f697928732d18b5e15.png"
        ]
      },
      "e00686e777acad3e27ce56a15307f162": {
        "fr": "LAPIN",
        "en": "RABBIT",
        "images": [
          "2f2543bea2f848b6220de6cee7b92379.png",
          "4a67edb15b8164a8bd320f9c4bba93a6.png",
          "5a190bf8167ed8065402e1d256111558.png",
          "6a88e56903c2abf22112d06e53b894b2.png",
          "7b4201434a1fc5ada3db873db89e2b94.png",
          "d75aa64ba5c8ea76abccb44914f37944.png"
        ]
      },
      "9128e2b7f7dd196cc03a3063ee6e9614": {
        "fr": "LAVE-LINGE",
        "en": "WASHING MACHINE",
        "images": [
          "038c10f01d8439f0f86dae547625a17c.png",
          "21556115645ad83f68815e0de9132138.png",
          "609f7aeb046e138e5dcb1600f3eb0ccd.png",
          "77112991f79b68a72e488ccb01824fe8.png",
          "e99bd53f087a5910f15b09e565ce89c5.png",
          "f4cdbb563f6ff9ef3e261f06fae25351.png"
        ]
      },
      "ef96fadcd38b7d981750d7a4eb05bc97": {
        "fr": "LIBELLULE",
        "en": "DRAGONFLY",
        "images": [
          "2d095d20ed1896e45128ccf8e354844c.png",
          "3fd66c9338ebeff3c0d00ffaf5a2833a.png",
          "511e7e51ccd112346f0fbbad42f09b2a.png",
          "84fe39e89737c45d50237a49e2289780.png",
          "a6df9b58209c12f70a6138cbc76de80d.png",
          "bb994406196b1138c67beaf5e3eb02cd.png"
        ]
      },
      "0ed27c703392458fc668c28b5412f2cb": {
        "fr": "LICORNE",
        "en": "UNICORN",
        "images": [
          "25df22b717ca4ad72caa34a878cc53b8.png",
          "28c26211670728d53d12f02408b23073.png",
          "4a77b0ec2a80ff6dad675c54cc8cd9f8.png",
          "5b5532bab69692a5b76a0180be29acbf.png",
          "9d31490b73ec38ba8ecfcd2caaf4cebf.png",
          "da4e341f0fb619bd084a28a12a135081.png"
        ]
      },
      "70b06daa7665f081c5ad9cc51c694f66": {
        "fr": "LION",
        "en": "LION",
        "images": [
          "05a23094f7ca0c7530d99fb40ec3fdda.png",
          "2a3736a3124a1335d30c5e3036bb5f2c.png",
          "32cfa944c02f7c65d517dfc9278b3d90.png",
          "334ab99bf13c4f43ce8f850731112a79.png",
          "5110def72f99b44abba449417ec09a2f.png",
          "66f96df89d44d7eede7269738cbff6e1.png"
        ]
      },
      "52b0232d7d49ba5791ff6fefb2daddeb": {
        "fr": "LIT",
        "en": "BED",
        "images": [
          "70519bf3f28442989418d682b6ae18e2.png",
          "9e0bac5c46f68e7f255e419ea9bfbf21.png",
          "cd432f1a69e23e242629a4ef99e15afe.png",
          "e0c85244eaa4a8e7493c3a7871e859bd.png",
          "f3dbe5061aeeb6e37d7dd0c358b8fdfa.png",
          "fe5fa515fecb1b56cc2713ca03a28082.png"
        ]
      },
      "e6b6d5ae7ce2f7c6c6d1a441edfa8d9c": {"fr": "LITIÈRE", "en": "LITTER", "images": []},
      "a979f6768f3f9388d327a289193085b4": {
        "fr": "LIVRE",
        "en": "BOOK",
        "images": [
          "0c4f79393a320c6c79f0cd081ad2eef4.png",
          "2219be25793e5281a0ffeadfb00088fe.png",
          "6b2a34fc31e7504ca6f49c770476deb2.png",
          "6cf7bc5df9a30cb4e0b94163676c9246.png",
          "a8e0d87df11e79bb0f7746c204ea59a0.png",
          "aa47b1fbe57e4c42a3a27e84795f7008.png"
        ]
      },
      "f202ab6e0a2ccfb603118e53e5e5ec75": {
        "fr": "LOUP",
        "en": "WOLF",
        "images": [
          "2bee7f76d5004f7439b49587870b091c.png",
          "461f41c54c749bbdd7208d6ede1ac4bd.png",
          "483f0fd224b570b294909aeba19763e8.png",
          "66b67ad434cb71c4b86abe34970afe81.png",
          "6ea70bbc0fe7ec93676bc58275bcb8f2.png",
          "e368cf87ad5794a42405f4fc750598e8.png"
        ]
      },
      "0862147e1cdc5b6ddea4e1f4bb9be262": {
        "fr": "LUNE",
        "en": "MOON",
        "images": [
          "37e0794cabf4d99fae3a382be0b6802f.png",
          "4d2a33aa7a10d74e72a2179435c73287.png",
          "5cdec25bc2e0a3dc6316eefd7f2486c7.png",
          "7eae0269132bbb4af5cf01f32f783f10.png",
          "8795743b66aefb02c1cd9fbe2840d804.png",
          "a810c18a8feef75e52aa84f43571b49c.png"
        ]
      },
      "a464d17a120c633dbf66b1921e276b4c": {
        "fr": "LUNETTES",
        "en": "GLASSES",
        "images": [
          "068d0e044b183cfc5fd7ffc509e176a9.png",
          "09e0aef1769aa7cc167831c48ea16da6.png",
          "5a658f5362851cc437ce62d339b050c5.png",
          "5caf8b0971db483072996e8b33632cc0.png",
          "75ea6a97077a087276daf943d75fd3f0.png",
          "8b319c0d36387b11d920e62bda516999.png"
        ]
      },
      "05485082b9b6e3900d6b9dfd70d82c4b": {
        "fr": "MAISON",
        "en": "HOUSE",
        "images": [
          "1c7ff89e88f99bde08e987cdf95d6b9a.png",
          "2f34d9731070ee65f31558302d21bf5d.png",
          "3a9a23bc46f6bcb977e4943e3619931e.png",
          "94dd3445a102175cadfbb30043af1811.png",
          "9e0f44e5edb8d4785ad9188c9a4e542a.png",
          "a7ff8caab1652b9901a368c37bb53d71.png"
        ]
      },
      "f7a1b80c233798e01c9a6f377d2ae253": {
        "fr": "MARTEAU",
        "en": "HAMMER",
        "images": [
          "01b65c8dd399f1e2714b3044c1c76752.png",
          "0dba397dce5ba29f4ca55b717d6d0e75.png",
          "28251116dcf3f986e0a193addc527afc.png",
          "ae24763e8e2d0dba1d698d9960021a09.png",
          "c54046311e22a142bb5e9c841294ff63.png",
          "d2aec22256514dee5c2c5c0fb0ae619e.png"
        ]
      },
      "e7b62672659b47c9815631f2f844ebed": {
        "fr": "MÉSANGE",
        "en": "TITMOUSE",
        "images": [
          "2e498684ff06bd1ce05b43f438a5d9f8.png",
          "3f85d1470c1515a8ed0be810d47f0a16.png",
          "538962b14717bdbc71e32c2ad534a74b.png",
          "5741b5aef32ca54016dc59158bc93eec.png",
          "959f461a5ad0b5eb878dbdf4dd7b6da4.png",
          "bf16ddf91936c2b675a1c264c10b4980.png"
        ]
      },
      "c2ff6b42a12fac06e638e539ea6528da": {
        "fr": "MÉTRO",
        "en": "METRO",
        "images": [
          "0161fb8f5f6414da161014eb8f7d850b.png",
          "0e583715e87768dc7e7dcb07e9df5eb6.png",
          "14422db539eb65c97f9042c9f8048e92.png",
          "4a231e2105967386abc8bf39e9c70f0e.png",
          "5bc1aaedd595657b0d66271cee4bf249.png",
          "8c5e6e154d02e068afebbf6d278c2c1f.png"
        ]
      },
      "9ace3f32a0b175b725b3484788283cf7": {
        "fr": "MICRO",
        "en": "MICROPHONE",
        "images": [
          "6cfa6b17a2017f6918745a415c3a5069.png",
          "84d67ca93077ab7f4ac32011a256048d.png",
          "8cd9a4ec4a55aba219f087e30423081d.png",
          "ccf551b2b9a71c55e0e09cfad38d2b57.png",
          "db3f9324c34595fa2e0933eef5f15219.png",
          "f2a9e3e69235297cc6837f8c2724c767.png"
        ]
      },
      "4e1fac40d29a2fe8ed14d451ed853394": {
        "fr": "MOTO",
        "en": "MOTORCYCLE",
        "images": [
          "2b3549119e59e1d566aac7eb7e0a03e4.png",
          "446618b7a56a2dc903f8a5e1dc2a6edf.png",
          "51d9411fa2ae4a3ebe014ca45af43b27.png",
          "884338b907e5a1d7d0c1faa1223e2751.png",
          "a0ed6c639e39792168e4b095c8f60e89.png",
          "b63f2f1b9befebd08a79e7690c35f5f6.png"
        ]
      },
      "3cdf4827e1cc441afa8d78d073dfe2da": {
        "fr": "MOUCHE",
        "en": "FLY",
        "images": [
          "0a0975ca5f8857c4d1245acc19336c3c.png",
          "646fb7bdc87ab897a970eb63f83181e3.png",
          "8da07da9c5a7155a6c90bb9c22213d20.png",
          "ae777ce5973fb370c38049b3cd0ae73d.png",
          "e2824732eee7fcd8f319b41bee1805df.png",
          "fd8bf9a2ba6e396b47e3b3dec07bf587.png"
        ]
      },
      "887073e3516f0db7e2dfbef420dc8a02": {
        "fr": "MOUTON",
        "en": "SHEEP",
        "images": [
          "3c1666df76969a49990c37dfc3140713.png",
          "6de47f50ad1a6de6f0a3205f3753e65a.png",
          "9e85b49ac7b631a4e7cdcf75720e5d96.png",
          "a9bb8cb8a86de7cd269420df04fe3eec.png",
          "cc1c97e52e8920bdb2056b4e26582e3b.png",
          "f7394d8364f4211aa2c4d231d27805af.png"
        ]
      },
      "80a54c0a6e374694225e7b6b53af4554": {
        "fr": "NOEUD",
        "en": "KNOT",
        "images": [
          "015939566a781240de62dd33a71c7b7c.png",
          "0703561cd739ba428e6e9336c1a1de99.png",
          "d6001f9d986186fce00071ca4430f0e1.png",
          "d84827ae931e71e7d9542500d2fcef08.png",
          "d8937c134a99de78a4bf4b0f5fb027ea.png",
          "e187e0b0af74f024ff9978c5f052d1fe.png"
        ]
      },
      "c251fd61686f9819884fd87ae0431ba1": {
        "fr": "NOISETTE",
        "en": "HAZELNUT",
        "images": [
          "43708e6c7c0177722f8ab21a2581bb50.png",
          "4474fe7f941df9332feb51ad43ec673f.png",
          "4c633f93afbf8fcb1b12e2f32595034e.png",
          "b4bed4b2c2a1b2c5cab1b132709ddac5.png",
          "b7aff1b35362d2461dddf097e398f6c5.png",
          "ce2097330d0db8c95d2995f9ac74aa8b.png"
        ]
      },
      "d9c9aabf9c3f03c22aecfed74a57452a": {
        "fr": "OEUF",
        "en": "EGG",
        "images": [
          "2c07d1d44e90a1df1006695315ec5554.png",
          "2e4633ce1446335000dbe991ff387c1f.png",
          "5809ba8fd851d4798bfd075f62448667.png",
          "5bb359dbf8031060ae5fd9b26b0952e2.png",
          "acc3f08f7531360df42bc3141bd71727.png",
          "ee61864a87b293e08a843dacc980f36f.png"
        ]
      },
      "7fbb1fc75e92a809a1a39ab5e2f29c0a": {
        "fr": "OIE",
        "en": "GOOSE",
        "images": [
          "16e26bc6efe7096b202a8bd5b33a6c6c.png",
          "2a91f0a2db336948c36e5d85b3d3a6f6.png",
          "31fc1c13f5a763c1306b8ec80777c5e7.png",
          "bf712c781088122eb860d8ed08aacb27.png",
          "f52d239b3133e9ce40c32fa66453c4a1.png",
          "f97280b7012bbcabedccda219d1cc872.png"
        ]
      },
      "ce83f167a7bf4ccba56bfd409436731a": {
        "fr": "ORAGE",
        "en": "THUNDERSTORM",
        "images": [
          "1e9ec03f4a1cc320266dfe203808bc6f.png",
          "471a1e02b69a81c2f93980f4ace27c48.png",
          "4cce1112881e0d6e662adecfb40c54e9.png",
          "6721217d673f652f56f66f8e456f0bc9.png",
          "ab8b354127bf116f39ea18a96532278e.png",
          "cc9a6660cda3d539f5c08e4bad1ca366.png"
        ]
      },
      "50d96eb5ac36aa88fa5248d675ae3b34": {
        "fr": "ORCHESTRE",
        "en": "ORCHESTRA",
        "images": [
          "3a64ed8a7360fca670f9a39e9feb484d.png",
          "42471a7c07c239f5631158de30c5c62e.png",
          "a37fd47110915879b48ef251eda71aa7.png",
          "ad4e064d4ebf6c3db4c2bcd1c5dc2126.png",
          "c44f6484fc39fa556d82e93c741536be.png",
          "f1f2d2d4fdd88eb8c78cb21adf4cf7b8.png"
        ]
      },
      "b620b50518799f0b87650b1859bb8200": {
        "fr": "ORDINATEUR",
        "en": "COMPUTER",
        "images": [
          "05c718e07c64198d358e6cf8bc8e8f67.png",
          "24508bf2b9ee21954e59d681a2c3f217.png",
          "68aa7c5f8467f7ec32660549d4e0adf6.png",
          "838abbf649b3565ac21cbdbaf9fe99b7.png",
          "8a1a0de26e348a094d0e0dcc65e702ee.png",
          "c035249ca12cd024b0964f9d34579f3b.png"
        ]
      },
      "1024215eeadd1765a57fa15271fea69f": {
        "fr": "OREILLER",
        "en": "PILLOW",
        "images": [
          "011d04dde28e83fee0d07baf7ed319ac.png",
          "6dd7867aa157002a144f9f5d3651379d.png",
          "6e6e1932919d5803b6766f56afc217cc.png",
          "77be051db7013e82fa4e3bae99fb99d9.png",
          "9cdbf70a2c9eb0c81d7b57570d1620cc.png",
          "a4a92ee4b14790fe233d3722d1dc0b4d.png"
        ]
      },
      "45ef0f32d2a4b2862fbe0c2d14753efa": {
        "fr": "OURS",
        "en": "BEAR",
        "images": [
          "13dcd6bd276cf05890b707a8c10f31b2.png",
          "1ae43b421fbab12adc9df81cdc28a1b8.png",
          "48ee1cfd70e88081559fbbb2fb83128e.png",
          "be9969c22994b061300959410b1114c5.png",
          "cf07864275a4b08180658386a8a287e2.png",
          "fdce94d47ed8b75cb6e2f52d4a166008.png"
        ]
      },
      "34568f8316232cf18831472099a9ee02": {
        "fr": "PAIN",
        "en": "BREAD",
        "images": [
          "135a00edc7592d9096d97461c0b1a473.png",
          "337990c276a0bf341a178f5b8ba30dec.png",
          "4c1fa18eb097c313b05f714440a2c4e0.png",
          "ace2c755126b5c6f85402ea5a26ce848.png",
          "d1c1ad274792bc95efd5b025d850aab7.png",
          "f3c47d0cd38dfb5446eba0fe184ed8cb.png"
        ]
      },
      "caa99c1b5afa0c8b1d47103d47122ae9": {
        "fr": "PANDA",
        "en": "PANDA",
        "images": [
          "013247b6f4970b8683c34b1e8912828a.png",
          "1b11a0595ebd4c099ee1c53aaf7f58ff.png",
          "394cdb691fa3b8eca0c729bd6ce544c3.png",
          "4f876163dd26fad75a23539a52c0ad84.png",
          "8b6e7f226f461f89fa6e55df8347b996.png",
          "c9bca1bb2924d95c88fee35233d1aebb.png"
        ]
      },
      "5ba06372f4d479e018c1a01ebb3ed4a3": {
        "fr": "PANTALON",
        "en": "PANTS",
        "images": [
          "4211c56571828f18ce60a818c5ce1915.png",
          "489da4e96eaa50c6b2633063644114c3.png",
          "6d6ca0a7638526d1e461231d3657afd4.png",
          "ce090207b8d36625920296f5a86daf94.png",
          "eb9261a8f52d26668545f9c518dbc090.png",
          "ef135b7676e8e34088d1d9a7277bd34a.png"
        ]
      },
      "545bc41ad35f184d063c22bac1bd6670": {
        "fr": "PAPILLON",
        "en": "BUTTERFLY",
        "images": [
          "633524aaa6b292bb359b4533c3d056c9.png",
          "797215809cd2263ac20119fe46e4186d.png",
          "7d2a4c7ac809437c79c348099d87aff8.png",
          "dff4149839436f9522521717b925eb59.png",
          "e0c9e0752e982b27a841f93160a6e902.png",
          "f3af1606917ecd1b6e10a3c1755847a2.png"
        ]
      },
      "9d4e465b25cadf1718cf4e7648044e5d": {
        "fr": "PAQUEBOT",
        "en": "LINER",
        "images": [
          "14fcb5e2fcd33aebb4db1adda6935c3a.png",
          "3fbb83eb9a2b5d87fc6df92c02a684ee.png",
          "a8bd933cefbcdd0386bf43ffa350f37a.png",
          "c998e3e71caf6ef6944e47735c5bdfd8.png",
          "e039e74a4e50a0619e5309e49627d430.png",
          "fa9b38c069e87ad9ba09da9b68e16a58.png"
        ]
      },
      "9269fb65ca03418bd79efcc4fe1f740a": {
        "fr": "PARAPLUIE",
        "en": "UMBRELLA",
        "images": [
          "0dcb037529b3b9e8598c7833746ecbc3.png",
          "38417474d7da0e8a7898e3fbe25950b9.png",
          "6735ea1c172be558d5e8a6bc798695d1.png",
          "9faa4228584716814168c55bb40bcbba.png",
          "d154209a8975d562c002576ebafca36c.png",
          "e15c8a9d576fdc231b71b6c8c5aa3b40.png"
        ]
      },
      "906917e611e9dad3880f583e6fd1d61a": {
        "fr": "PASSOIRE",
        "en": "COLANDER",
        "images": [
          "2c9135f99579ad9c4508c0b083421dc9.png",
          "5193459cba93e80b5c57e3bd6d8ccf7a.png",
          "6a6800373d7e453f62f042227e8af4b1.png",
          "ae78f578ff178ecddb685d539c4439e5.png",
          "ca2c0486f22ff9b723da60869bd544ac.png",
          "cef0fe6c341b2409000a2797c2079247.png"
        ]
      },
      "6725958611b30cfd8f56bb971cb49399": {
        "fr": "PÂTES",
        "en": "PASTA",
        "images": [
          "0b01d65805235320b708da2a15ce601f.png",
          "1fe1ee481a805420c1e03a55654e8d15.png",
          "634937594bc1f75362d3e91e515c4669.png",
          "8c4d8d4f7dd93318ead3fb818712aa86.png",
          "f6bba7a977e6160e39b6a0dee3aa34d7.png",
          "fa5f2e206fea0626d1aac985504ed8a0.png"
        ]
      },
      "7d87ec1d86562857271adf4c64c6b5f5": {
        "fr": "PÊCHE",
        "en": "FISHING",
        "images": [
          "0907a7653ecfb6014955b668bbdd4720.png",
          "1882c8a0fa8b90eff543bf67c16ed721.png",
          "5ee04ea3607866ce1cff75de29a8991d.png",
          "6baf9c66b764d53bdc9f11263205edbc.png",
          "7cd1974b57af2cdfe1de4724e04b277b.png",
          "8538f8e30be243b54bf303793f777793.png"
        ]
      },
      "2d58c425a9323ded34e700591f34d8be": {
        "fr": "PEIGNE",
        "en": "COMB",
        "images": [
          "09da6a6f59e58a0d1b31767698d866c8.png",
          "3ba9c22e243f4fd84e491b6a27123f70.png",
          "4f125bccd5662a81dae7a992592ab19d.png",
          "9c24cc51e373c8fff2f2a18a3e0d3d83.png",
          "ab90a36f51d5a29fac85ea7d53463722.png",
          "d55aa54230420a190bbea9e2f6290ca8.png"
        ]
      },
      "fdd4cff55080694ae822d823f2716378": {
        "fr": "PELLE",
        "en": "SHOVEL",
        "images": [
          "6ef0bf03bfce3c6c68587207ed660e6e.png",
          "73895e2cd7ef950dba76481036a2af28.png",
          "7994cf5f66cccf34cbef8984d73f0b46.png",
          "86f49454a0642f3362b634eaa49d445e.png",
          "b917976f95045fbe880d8110a93241f7.png",
          "d14cdca97ecbc2df333230c64207466f.png"
        ]
      },
      "c960d8ed4abde97a095a6e8286b6f5d0": {
        "fr": "PELOTE",
        "en": "BALL OF WHOOL",
        "images": [
          "2381b7e0178ab403866736b45b7f742d.png",
          "293ea1f03607d34070ccb2d293d3ece1.png",
          "3e93438452b4ba35dddab0ecd6037d0b.png",
          "422c6b630386e03733b343ce6083646e.png",
          "7f4ecce6a5c995db39f6b46a1b81e010.png",
          "ada4401faf34b5869567f0dc2c30c238.png"
        ]
      },
      "6517c1a596d36a060671a1d4725d8fd0": {
        "fr": "PELUCHE",
        "en": "PLUSH",
        "images": [
          "1c102fd52a2f5ce184599cb5181abb1f.png",
          "4bafdff0dc20ead34be2aebb2728c6ff.png",
          "4bbea42a57255c928ca175befc3e5fd3.png",
          "59f2759cbb4bed65ab0b7f39932ddb7b.png",
          "6e7e92df04b05d1c5368769a33403b71.png",
          "cfbaf8a7e0b4bde49d6151acbec11277.png"
        ]
      },
      "663bdfc6b7b93c5b74886c964c200562": {
        "fr": "PENDULE",
        "en": "CLOCK",
        "images": [
          "01be0df8f7f08b84ef64a7d10c6dc4cb.png",
          "4ceb0c7298d7a09cef1028f562dab8e4.png",
          "77393ea3c31ecb83d0c990cf2d46f848.png",
          "bd810cca6abdf791dd3eaa994b0e74ae.png",
          "ca29c0db71f03f251f9b8b64fffea667.png",
          "f1b3336432de633ae835aace63a79a6e.png"
        ]
      },
      "c7a065ce005f3ff62904c39ad51b0d14": {
        "fr": "PÉNICHE",
        "en": "HOUSEBOAT",
        "images": [
          "1ef97248e1c158693fcac9a0149be1a1.png",
          "2c50d8f795116e076d60d15bb64c1702.png",
          "63e7087682dacc6e11ded469882edb20.png",
          "82af943f0707847173314ed096073af7.png",
          "9b1951ee764031779baff50417369343.png",
          "b03f14cdb84878e84cf618a9fa1209a4.png"
        ]
      },
      "943374b4dba32d411f53f3cc1198e7a6": {
        "fr": "PERCEUSE",
        "en": "DRILL",
        "images": [
          "2c1f1831f8ee88fb78960cbe25d1991f.png",
          "2ec1faaf7bc7c01942a3c86986bf3d69.png",
          "37b7a9136ba0749edaef3472b1c10cb0.png",
          "6be72ba71b83749272f1e306ba2b48c7.png",
          "9243b07d5852d880c5fd94df3108777d.png",
          "e58dc07d1dce3f6681e2b51e034bcd15.png"
        ]
      },
      "0121e09f1d7e73fe14d048d5fe9cf674": {
        "fr": "PERLES",
        "en": "BEADS",
        "images": [
          "181564176a2a32420641c972517fddb9.png",
          "30194f4ad65d650678c629c63891c0ca.png",
          "94694643fdcbebe7540b9a58b005f380.png",
          "e13de6364e046db032cbcd7cf0c664d1.png",
          "f31d2c7ce4d630cffc30384fbd352f36.png",
          "fb1c89674b848aa3e1d8f11b9a11d6dc.png"
        ]
      },
      "6b7adeb8b8df0eba6b960e3a22393167": {
        "fr": "PERROQUET",
        "en": "PARROT",
        "images": [
          "1233a0ebda1fcf51f4b600962e546575.png",
          "2718b4796b2ad3536e7cfe2ad7d12031.png",
          "291096b38cec70d4f69a85726c8206d2.png",
          "3a72d49205f85e9cb2017a8dd60708ec.png",
          "5522cbcdf1180fc0cdd3a739cd70bb19.png",
          "6af7c87d67b1aefe6fbad5465894548f.png"
        ]
      },
      "9eb93d1033dcd4b4b06041aa7dc41a35": {
        "fr": "PIANO",
        "en": "PIANO",
        "images": [
          "0c5b0ba31dc1d95f608da7028583a8b3.png",
          "104fbe949dd825226743f431d3f2dab7.png",
          "ac0ce367da7a700c15ebc529238facd3.png",
          "b675a86c38f65ea92531cd665653fcab.png",
          "bdbc783ea929175fdbd0e4020376c8d3.png",
          "d486f3b07bc69696016db2abb0253246.png"
        ]
      },
      "2136e70c58d15009695ad59f2f13c262": {
        "fr": "PIGEON",
        "en": "PIGEON",
        "images": [
          "15f82d685aabd0591971f2cd1789e560.png",
          "20d91b52a8b1a460dbb256453a646bcc.png",
          "46fdefee6fd9d5968cdf82c5233c1290.png",
          "5cac5f15cb25bad09fdb3c8af5eff2c6.png",
          "d899804f03ac157093c4bb2036fc7201.png",
          "fedf2e563cbcb30c726c8b62c9186676.png"
        ]
      },
      "8054dabe096cc450dd4589e683b0667f": {
        "fr": "PISCINE",
        "en": "POOL",
        "images": [
          "46d62dfb75de6ccc084fbd699552dd76.png",
          "6ef2fe6aa4c30f0d3412bcb86fa1be45.png",
          "8a376012bdaf197910259e769cb06bfb.png",
          "bf6f20f67aea85d85d1f304d4131f17d.png",
          "cabcd9d06ee99dfdf2efb833164beaae.png",
          "fe2e02d969fd12edcbe78d64ef18cc15.png"
        ]
      },
      "ec395f360604b2e5aa479c9d1fd7e7c2": {
        "fr": "PIVERT",
        "en": "WOODPECKER",
        "images": [
          "067a0d4c3f3722f987faa2e0f8e68d02.png",
          "627f0383e7f698c92a4748719dd37b2d.png",
          "6fe8dab998edc91bfabc429cbb37c0b2.png",
          "7d5e82cf47ba22679694caa2d611d2d2.png",
          "849e0dbb964948aed90730c1cc1359b1.png",
          "ff581a19f3b8434d98f0d98541c32429.png"
        ]
      },
      "61859dadcb2f0cd79736753389f7d35a": {
        "fr": "PIZZA",
        "en": "PIZZA",
        "images": [
          "024c23d026983afcb96cf1aa49db044f.png",
          "15d46250d2337c49e9e199632fb14263.png",
          "1a622858ada488213877e2a583e75f8b.png",
          "29e3dbf367d596d42125820dc318ee29.png",
          "4f6435717746310fb306bf0b2dc6ef05.png",
          "8d2af4eccef8d776a91ca116e8e0b44e.png"
        ]
      },
      "b51ffa698f144716f5217babbd137248": {
        "fr": "PLAGE",
        "en": "BEACH",
        "images": [
          "0ee53035b07737014aa084d867f9d06d.png",
          "2105bdcc1f55a3bf6025fd0415c5fd9c.png",
          "2acfd7e5357e9c18eb76ecfc2d4fa520.png",
          "a299122130f8f96cc2aa9bfe732ad8ea.png",
          "abf7e65a9d14a175bdb68b9abf2b942d.png",
          "b4ec484b6ec065e722f6b645c4674fa4.png"
        ]
      },
      "7d50ae1e7aa5922b66ec6ca8022d273a": {
        "fr": "POIRE",
        "en": "PEAR",
        "images": [
          "3a45e70f50e4b2b46817fdb40f6aa0fd.png",
          "3b7f9a50f336c7e2cc7520043c923f01.png",
          "81ce2661689829cb8515efa7f060c7c4.png",
          "8d294be808a622d55832ecf16a5c7bf6.png",
          "a4683413eaad3fe0698811a98d75803c.png",
          "f6b0577eda2d42313d204bda548560ba.png"
        ]
      },
      "b5e2cae0d0dd3dff26782533ee42e195": {
        "fr": "POISSON",
        "en": "FISH",
        "images": [
          "09a596f859a15ac11c2b3a5d3a7eda31.png",
          "18be45115650300d72ff15f1e5b35ff4.png",
          "1ee6525973ac86400794929a22470e05.png",
          "31d6ee7b322e898659801b1b02d89ec4.png",
          "c8e57aeedcf255898b3ec96ef6e364fb.png",
          "feb39fd8b7a08c9275490b356b1ce766.png"
        ]
      },
      "f5b34c11d25e3e7b3b03728d57abfd01": {
        "fr": "POMME",
        "en": "APPLE",
        "images": [
          "2ec67ebc37679151ede73ba446ed6867.png",
          "325fb0e2a3f7e5cad397f8e900f61455.png",
          "54885c37e2905bac507a7465c1340031.png",
          "74e6252648cf1918a75a91be471b28e6.png",
          "a6bdaac224bc824bfe2761b24c124f34.png",
          "a6fa636da05a9250c9c832ff2b6a846e.png"
        ]
      },
      "1ca422a16f111614d844fc2598ec5665": {
        "fr": "POMME-DE-TERRE",
        "en": "POTATO",
        "images": [
          "0c539346fe364f18764b857879c9cc64.png",
          "0faa93d780f6c85f2ecbdb7f16568a49.png",
          "109831beea6b541d1430fe23c3b1a7cc.png",
          "29e6bdbd27d164f3e791f5df406d5aa0.png",
          "978b47e2f832648f6cdde24001f2680c.png",
          "981b38ebea6e40433d81cbc35d9e121c.png"
        ]
      },
      "acf1c3fc112e02cb5eb54517739df049": {
        "fr": "POMPIER",
        "en": "FIREFIGHTER",
        "images": [
          "1c9c2feaed807266a29a2eef38536b11.png",
          "1e1c164d024d261f922dd28c4afde827.png",
          "6057419d4d70d6a448a49c029714476b.png",
          "b0a1bcdb395d524b2eddf851cdc16a3c.png",
          "dc638d55d1f2430b126abb195ced5039.png",
          "fa3e481440324a498dc342bbb76f67e7.png"
        ]
      },
      "01f71faa707e181a41b3d332fcf1fdf5": {
        "fr": "POUBELLE",
        "en": "TRASH CAN",
        "images": [
          "4be7c1593207cf4f3f0304d08dfeff2d.png",
          "4c48afb87d1022b740babc31f60894d7.png",
          "75898db84cb801d200c4b835f6634bc8.png",
          "78a6f417850caf34369a77bdb6fbe0cf.png",
          "87e8653298ca4880eb6581d97c9b1721.png",
          "b311c4fb80b0d698657d11814236f585.png"
        ]
      },
      "d71ca4f2db8456654e9315a640f7f3ad": {
        "fr": "POULET",
        "en": "CHICKEN",
        "images": [
          "1975eb728ba9e62be38ba2c9711a921a.png",
          "956f77f1fc0de43755d789909f640f87.png",
          "ace1af0c617b675cf9f093b60a3d0dbd.png",
          "ba446ba65f9280623f315f9c78758d9c.png",
          "e914c54b60734e3c6ae8dd1c0f2b17d2.png",
          "e941bfdc6b8fae9bfa58317b11582a5f.png"
        ]
      },
      "4e7084d0cb6215d2a03dca4baf2a2973": {
        "fr": "POUPÉE",
        "en": "DOLL",
        "images": [
          "1d1af92e7ef1a43b18b8c509d53fdc89.png",
          "7f199b0ee1b04ddbc41aed5b8317f30d.png",
          "92a1813b0d22552ec0aea6a85d439721.png",
          "9b5d00ac8fbf0cc1064edffeb9f98bfa.png",
          "cc968ea71507b020b04c9c07674773d3.png",
          "e2c32ccd5f8aee4d12cd6e79ff74d655.png"
        ]
      },
      "780ccdebe749effcf64bb55d6bea3e25": {
        "fr": "POUSSETTE",
        "en": "STROLLER",
        "images": [
          "1a3ca7681fd018ce0a2e63e90161dcd8.png",
          "1d5e25f2b1bec8690bfb4383234779de.png",
          "5a03649d5fe6c791c631a66f17ab490b.png",
          "845c4ae159797d516c6130cbdcff5224.png",
          "8cacba3f2f89e01371896853a5c09cfa.png",
          "9b3444167405a1dccfef33296fc7e9b6.png"
        ]
      },
      "82cee9c270ba745b420ddba4584ad004": {
        "fr": "POUSSIN",
        "en": "CHICK",
        "images": [
          "06d530cdff073baf59ceabb4ea8a8902.png",
          "25fcb10de372ebdb03e37a231fe80a79.png",
          "2dec77db4825f7e572552fb50720b9c1.png",
          "7b2cef2a2e04489648cd0e9c63e7718a.png",
          "cef70e5c564501b1ae4854e343e1e6d3.png",
          "e47259a508e7f27b4d72aaecd3b5ae1f.png"
        ]
      },
      "3fe2c51a954d6890a880d39a2742e3df": {
        "fr": "PUZZLE",
        "en": "PUZZLE",
        "images": [
          "7eda35a3944af64cd902b28590845d67.png",
          "af540ffd169965adc510b8a141660634.png",
          "b9bc4897c4252fa7664d3a81276dc41a.png",
          "c4a3b7d9d836623b07d00a273139916c.png",
          "e570201a492be50714679444f12bb73c.png",
          "fb78381ae925e961540329a0693f31ed.png"
        ]
      },
      "68ce22512e0a6f60eea1ed152bfaaf08": {
        "fr": "PYJAMA",
        "en": "PYJAMAS",
        "images": [
          "085d7839ce6e6ef945314c6e3318355f.png",
          "0bd368c31c784fdbe5f580bf95275cbb.png",
          "0d9f9d81eec3912c479a9c0007690319.png",
          "5b0ba098fbd40a2f19134d0147dc2d2e.png",
          "a46810d6e0c4312dbd3e27985f68f9db.png",
          "c3684c0064e0053bfbc5de6de4e99694.png"
        ]
      },
      "78110fc105865b8bf27f53125661272c": {
        "fr": "RADIS",
        "en": "RADIS",
        "images": [
          "13ef5da5c83ecb083294e8155cd0bf1e.png",
          "702bffbffe7440ce5a78ecfa36b9696e.png",
          "840ecea014ea70531c5a6fe593e72542.png",
          "c318f0d17f188826d81cf69890004ea1.png",
          "cd52f8f372a59d2b9dfc22c0ec76311a.png",
          "e97cfc0d170f425a5792197569965242.png"
        ]
      },
      "544115e30f5d539cd2e98b647e114818": {
        "fr": "RAQUETTE",
        "en": "RACKET",
        "images": [
          "0c52c864350ebc830f080739ceb07cb6.png",
          "2351ccaec6859bbf3c09564584ef3e6f.png",
          "4f9aec3deaa3d7e5033b5266a2056128.png",
          "a4aa21725d20498051400483af9b8534.png",
          "f9792f332c17491ce483de9d9d81a718.png",
          "fc98f73f074255af588347834218e5ab.png"
        ]
      },
      "eee937ee15965561f74e0d9bab2dcd64": {
        "fr": "RÂTEAU",
        "en": "RAKE",
        "images": [
          "6d0da108b38c5e90d90ec29da82292f7.png",
          "8b22609754bb2ae01b7628deab405d13.png",
          "929fa6f2e2d629f10dd647d551419486.png",
          "b9bdb08b07306a6959bcfe7909f65b66.png",
          "c25f5ebe0c6bf67c13f575542f98c43e.png",
          "eb1c0116145c26e6d91a0fec9e4f2555.png"
        ]
      },
      "b9fb2db9f25463a0e721611e0d3090d6": {
        "fr": "RENARD",
        "en": "FOX",
        "images": [
          "2f7d579445ab09c52c403b44c36b9a59.png",
          "49bfcada44ae1b472231350c85f8066f.png",
          "56165d876271db96d96582b88d6ec074.png",
          "c631e3d47da8b86f28077148c7242fd3.png",
          "ec4de26308549b21d3d045db734df304.png",
          "fe601b5832cfbd49ac5445b4c9f781a9.png"
        ]
      },
      "0ca15d110279eaa9c515cc7a12fdc906": {
        "fr": "REQUIN",
        "en": "SHARK",
        "images": [
          "08d69002d3be6e470d04cff5d543e602.png",
          "136b07f172f989f221ef378de042623b.png",
          "3e55b8936671f295e08ec457f2efa635.png",
          "42136685326e7c8cf3ab198c70a3e5ae.png",
          "67aa271512b62ed0218fdb669a956301.png",
          "89690448fcb029b2cd0c494990624de0.png"
        ]
      },
      "c2ce0d533950899b60123934b6a07ce6": {
        "fr": "RÉVEIL",
        "en": "ALARM CLOCK",
        "images": [
          "32b20d05609517fd989858c6c0504ab9.png",
          "70c2e0d6bcc06c661a6b0f188762d558.png",
          "7e22504bcecea3c6570a3e54c6a0bbfe.png",
          "9b4331ad12052f6b690f84cadd7f51f7.png",
          "c7cede02a17792e9824caf694279b420.png",
          "fc11fb53330ee73346351d18283eddf8.png"
        ]
      },
      "56234a48f4bf6e569bb12decd00b0860": {
        "fr": "RHINOCÉROS",
        "en": "RHINOCEROS",
        "images": [
          "0ad30fd4428a20fe9973c7a9555a2fa2.png",
          "299196ce463a3969525e8fcd50e99baf.png",
          "594bfc5a0b0edd014c866acd98d41c17.png",
          "b768c943d3dcfd4227ac9044d3a83872.png",
          "b85f6afcef6d73a329c74f64861a34de.png",
          "def62cd14a60b8fd7d399e58de392be2.png"
        ]
      },
      "fdb3abbd3b9543e98c9ceebbfa244061": {"fr": "RIVIÈRE", "en": "RIVER", "images": []},
      "caa130cbf2185322f809ea875d7be2e9": {
        "fr": "ROBINET",
        "en": "FAUCET",
        "images": [
          "35748a46f7cf0d8ac985a88d861f31c8.png",
          "4db551ca471cae2e9c778201251d3a33.png",
          "89f0cbdfe273213adf373026e6937bee.png",
          "b2f8dea6d2fa66a9f77b17ebfa354afb.png",
          "d982c3d8b6b1420f8475405bc464bad8.png",
          "e3532fe4fc28372bf3e046c5aa302081.png"
        ]
      },
      "bce1d162395546c61823a9343dce935e": {
        "fr": "ROSE",
        "en": "ROSE",
        "images": [
          "210c960c04c6bdd6953d92d6a5f3f2d1.png",
          "3503d611d99294f9e1bea974551de94a.png",
          "6264a79f7365888de5c45a8ad92542ba.png",
          "6908e8bd31f4ee95577e57e639d170f7.png",
          "75820ef8896f24aca05d1b5ac695d5e4.png",
          "9e2ebb4085981e79a7909bf4152d7cc3.png"
        ]
      },
      "62b6db307503c011ffed5729734adf75": {"fr": "SAC À DOS", "en": "BACKPACK", "images": []},
      "a3aedb47ec896e89b6ea38a2c38e92e7": {
        "fr": "SAC",
        "en": "BAG",
        "images": [
          "0790f7485cf89719b6111b79c8795b4e.png",
          "40e746a3574d2ed5e8267a6da50626b8.png",
          "67370943fab4d95a54a7faf781be17c3.png",
          "bfadc26b7498050e626887cc34856122.png",
          "d1cadf7d3066e066717e024fe3274f41.png",
          "f3e6ec9d97f66f4e2defdafdd86465f1.png"
        ]
      },
      "63cd8c0af50ab31fc35ff2e70d91286a": {
        "fr": "SALADE",
        "en": "SALAD",
        "images": [
          "0fa4f29b412b7fe88a3b2e455fc27b98.png",
          "37cf75a15089a7966e9a447b541f4f7e.png",
          "779989a935da9dcab194ea15f974f0ee.png",
          "88fd92952ff0e0323fac494c87610818.png",
          "a61dae3dcea48c8958547efbfbde5c24.png",
          "fc17078494779518e4d7fdc2bfa73800.png"
        ]
      },
      "fcdbd16a8fad6e2abd7298278a47fa25": {
        "fr": "SANDALE",
        "en": "SANDAL",
        "images": [
          "47204df56ca1dede919f23993affea37.png",
          "5cc71e79776b2417fbacf4df35fac31d.png",
          "a0842ba467beb75a43eec59dbf264e4c.png",
          "c178e8957ed756d9e3d159dab0dcd2ed.png",
          "c8f13f56652da12ce961b108a804dffb.png",
          "cc3ad569f8f1a70a845fd8e0648d1d88.png"
        ]
      },
      "7bad476a67101524ab2220f0d46bd542": {
        "fr": "SANDWICH",
        "en": "SANDWICH",
        "images": [
          "37345ea399b6dde7eaa222ac4e2f17fa.png",
          "78f35dd3f778f75d220e78a331f6e1b9.png",
          "8fab12bdc942cfe8754cf800aae9365e.png",
          "957a6c7a95f86ad9fd26a1a334b10db8.png",
          "9c7df055df7a27915e28ed8bae223931.png",
          "b6865caecda0cce019add0ddef5327ae.png"
        ]
      },
      "2f7069f15232ec0eb26161dfb4e5abcc": {
        "fr": "SANGLIER",
        "en": "BOAR",
        "images": [
          "3d051d4e319ad4bc36d7ed101376ef98.png",
          "603652f9a3bbf2620a800fd4a08eca87.png",
          "83c9821bb527b8ac491d0a7affd18dfe.png",
          "ab876ecfe7a44a6ce4a92a6044fedbff.png",
          "ab9bdcf4e9dffbe03d774e46a6bdb471.png",
          "c877e36aa756212a406009dabfc21779.png"
        ]
      },
      "94f547356abae3a8823413ee2ad70f99": {
        "fr": "SAUCISSE",
        "en": "SAUSAGE",
        "images": [
          "017341fe464b36a5fe0d210022c913a5.png",
          "05f802a59f16775d9a8c360939b67da9.png",
          "24acce8b5ae71bb6f0f44f0008cb53d6.png",
          "6f8b533814e1c357de475506629d83e1.png",
          "73b14a45fd44831ca54c4d2d6e6b6ed3.png",
          "b9052c98a60aca0a9300ebdd1ede3374.png"
        ]
      },
      "7378b7ebd325588cc2ca397c740ab9d5": {
        "fr": "SAUCISSON",
        "en": "SAUSAGE",
        "images": [
          "03a00760cae0d9590cd9fc632dff007a.png",
          "5c75003531393aeadbdc98aa2421967c.png",
          "663f3df1651cbafe32c3ea8d6e8d5f0f.png",
          "b0f2f8a0a10d1515d8d80e19e04a04f9.png",
          "ce8208f2c91afdb5f934b37d754908a5.png",
          "e9e86d6d81cd912233fb52cb1014a665.png"
        ]
      },
      "21d1d8dbc094950f2b13cc932afe5059": {
        "fr": "SAUTERELLE",
        "en": "GRASSHOPPER",
        "images": [
          "0726a67681802dfb32d00c6cab6cce6b.png",
          "0dfd3984b7cacaa0b8a82665dee447df.png",
          "1e5d4fee0ff854bcc507aee21592a3ae.png",
          "3491f3fbabc5be7351d54c5b06a209ef.png",
          "5e6241f09d77a8870c6afe138aa55fe5.png",
          "8db6fc34f401d130cc01e363a45317e2.png"
        ]
      },
      "1da5d865348e730ca533c9e9f5e40952": {
        "fr": "SAVON",
        "en": "SOAP",
        "images": [
          "30180e03ba73d92cea38803f814411a9.png",
          "3b647d8dd80806e34b01e3a4d06ed53b.png",
          "60d64d3be0e0351f4ff50c72ca96ffcc.png",
          "b5be77ea37a0b613d0e14895c0943649.png",
          "d430c5865acc0a05ecd85e2b5fd49903.png",
          "ed5efa0b46b842fbb1fb3ebe85ce04a2.png"
        ]
      },
      "0464abcfa938bb54a41e97b833d65633": {
        "fr": "SCIE",
        "en": "SAW",
        "images": [
          "03976798126ecaed7dcbb9acc10836ee.png",
          "04931bd56386d61806c7dddf8865c420.png",
          "7d380a42b22850efe5f06113dd702b56.png",
          "8994eb67186f16e7dc099058bb2e5cd9.png",
          "bedec6c6c6a2f424c1bad58de43533dc.png",
          "d9e66839135ed448d3e3b6ca2a0292a9.png"
        ]
      },
      "2ec6c997b9f6fa84854b4178e299b0d6": {
        "fr": "SEAU",
        "en": "BUCKET",
        "images": [
          "62b4af179b071018d255c4e41e17629b.png",
          "850ddf8db736bb28db32cac6ac80adca.png",
          "8def1a06e1296df24867377dbce6a49a.png",
          "9e2ce58d17f52e4d147871586ea1e5cb.png",
          "b52058ebdab22bb592aee3ee66e387fc.png",
          "c7c123fcfd78edd496900506e565a5d0.png"
        ]
      },
      "9d2e9fe7bbe5f920032c796daadb41ad": {
        "fr": "SERPENT",
        "en": "SNAKE",
        "images": [
          "25b1c8577e7f12d29aa1859ad4156261.png",
          "54f9dec26029cd69e3774b0e7f2aeb46.png",
          "63275f62405f2065898dc519e8580d27.png",
          "a19d235ad48b9921778fd9e93695f123.png",
          "c1fbc2c4d917d629f3cea507300386dd.png",
          "c8966624dd8bbfcfb8e6dbf00baf1d8d.png"
        ]
      },
      "0e0fb42eef7d5f20189bffa3efca70d5": {
        "fr": "SINGE",
        "en": "MONKEY",
        "images": [
          "7c3ce47559f06dada4ea7948bde0e832.png",
          "941703f8689ba624466676033b189af0.png",
          "9ba07cf6aef4eeb72d5ffdfaf0dcb883.png",
          "c5a6fbd1176ee93a6e071ab46f31065d.png",
          "d79165699f0641b38cc1744b8aa31b72.png",
          "e683db8d1c59de3471d48e488ed0cfc0.png"
        ]
      },
      "ce2cd259d5235dd721ee5df694a97ff6": {
        "fr": "SOLEIL",
        "en": "SUN",
        "images": [
          "03c14a69013d9e822e926206b3352ef6.png",
          "1071ab6686846ae0ae46650419ac2c9b.png",
          "2402c3faba9a3c436d7618889b2adbf2.png",
          "3cf15abe144a3aef8162d485f34c08c6.png",
          "49bda09ae9bfd2d15c8da47564906548.png",
          "e9290ec0e32d02480b4ef49d7554939d.png"
        ]
      },
      "c425f48da547219e3039d51031c25225": {
        "fr": "SOUPE",
        "en": "SOUP",
        "images": [
          "23183624c36b38972609d5a61ea7d10e.png",
          "55cf6593afa7ed2463eefcdf9c21c196.png",
          "7e66797df786fead565365b8afe3e78f.png",
          "9dac3c14721332f19894d44bb7d3308f.png",
          "b745d978f35fddc1cc94ccb71b9a9435.png",
          "f7986c54d058046f85c008730ebb3209.png"
        ]
      },
      "e4d5360df3a94ba6ee2095ef8209b908": {
        "fr": "SOUS-MARIN",
        "en": "SUBMARINE",
        "images": [
          "4c9f8d07645dd03eaee9dc715de21ab3.png",
          "4ec102f516dacd76e220146035ce37d6.png",
          "76ed34b12e59ae8b31a64e6fb432c876.png",
          "92cba2216e197ab6bef06227c648d5f6.png",
          "b739b3fa6cff6fde4c78e60004b00ca3.png",
          "f9a404e360fb738b6e97a349a06b183e.png"
        ]
      },
      "05308b74ca0d9461d89edcf5a1efcf44": {
        "fr": "STADE",
        "en": "STADIUM",
        "images": [
          "070196f8ea0b6963064fffc1a884a0f2.png",
          "1b686885764aeac89ea5c6df9d81fa0f.png",
          "4e9cf09de4af045238ef1a97ad15ff32.png",
          "65dc993837e2df50521822f1db89491b.png",
          "893b7484b38f0e85f59c4b0af482da90.png",
          "b488af30fe418fe48120d2a05c2f6c9a.png"
        ]
      },
      "42efafa8a7a1955da10c6722964403c1": {
        "fr": "STATUE",
        "en": "STATUE",
        "images": [
          "40f07c156fa416900afb1a1b996f5c1f.png",
          "4b91f977ff6337d77298547beab6163a.png",
          "51e4aac05a8167ea411a57dfd3fc5f69.png",
          "5aa1ac343e57caf11dcd3e9280ba7fae.png",
          "6a098d76b45797551cc57613a0693d5b.png",
          "e7b205e18caeada0e3e49cd060bee44f.png"
        ]
      },
      "40f91e9be28030779c36e12bfc4cc270": {
        "fr": "STYLO",
        "en": "PEN",
        "images": [
          "039396b6b2c4f81765ecbc5fab34060d.png",
          "04c956e01894e24979f994a67591777c.png",
          "0c63783b35c1a01297e29f3c6c415bc4.png",
          "504bacf60b10a5c031e8dabe70b459d6.png",
          "5669f5e926e20b80f08c22659cd4c6a3.png",
          "b5b8c0f51d9577f205e7023dd2dee46c.png"
        ]
      },
      "f2c8341cf41f5d830bcd977d053a2430": {
        "fr": "SUCRE",
        "en": "SUGAR",
        "images": [
          "21d2cd56f197b680b35ad89b74b65899.png",
          "3f986efbf4c9b968c4e2693bd0df6e69.png",
          "76d5f2f7cf07ac17094397ce19c621be.png",
          "7bac317fe18ed4382078227556759696.png",
          "9119313531caa3c0e43bcf4ebf6bb915.png",
          "c7266e126c1d812f9d8c85ab5a0161fa.png"
        ]
      },
      "a57779911b5365509d591bf353a3b9e3": {
        "fr": "TABOURET",
        "en": "STOOL",
        "images": [
          "1f9c7c30d9175c5744d7edbf3cbb97a1.png",
          "25807688d12d730963dd90249b726fbb.png",
          "66b7a536c057afbd92c27f84970e50ff.png",
          "6d101af8371b76b48ea80afced6eb5b7.png",
          "8b394b909551480627c9787cf6ec7496.png",
          "8e8ed4c2d052914ba4f661a71591bae9.png"
        ]
      },
      "639747f4431225fa6fc4ccc9d4892083": {
        "fr": "TACHE",
        "en": "SPOT",
        "images": [
          "03c2d6222e8c9ead99be3a7118e4ea84.png",
          "211b80a7b3ac21b8526b7c166ca9baa6.png",
          "67a89474730dbc21023e42c295c22fc5.png",
          "766b656e8bb782d0e6a7fb0f961074c1.png",
          "88fa0ede514ea49ed9ef63f8256edb32.png",
          "fc56ccc078c2ed7cf69c808c6c4bea68.png"
        ]
      },
      "c0e96311f5eb18c3f170e9a18e844d4b": {
        "fr": "TAILLE-CRAYON",
        "en": "PENCIL SHARPENER",
        "images": [
          "0858a3ca4fb318bf421da4a19c119a9d.png",
          "25862fe2475a98f948735c110be5e257.png",
          "299cb46f28fa8ed44ea58fb53bb4c82e.png",
          "3053389cdc93859312d716bbfc295820.png",
          "54d8feb1c0da05f730ca5396e9c29ff4.png",
          "796f02000aa8bf135a9ee864d90be3c7.png"
        ]
      },
      "ed8381647da1faab0724cd12f4cb369f": {
        "fr": "TAPIS",
        "en": "CARPET",
        "images": [
          "38485d3a1c334c968dbd56f16a0060ca.png",
          "622c897d8ceadff95f0b20f0f1c7acf9.png",
          "8f702f0eb59dc881ffba19a2447bbd15.png",
          "9b7ea5649fc884f4ae38dd59e27f074b.png",
          "9b928b1f997fb74fb30a6da4e07f5406.png",
          "b7d061c800a35dcac9ee5701bcd02624.png"
        ]
      },
      "852269a0d1a8d902571c1c368595a6ba": {
        "fr": "TARTINE",
        "en": "TARTINE",
        "images": [
          "21530469879ab1b89a42beb85193c4d4.png",
          "2da991cb289c6eecfe3022a1e1b7ad19.png",
          "71515b33a20fb8c785b7a16a9c7a8de1.png",
          "c245cc0e6c1dd18315bc4d848884aaaf.png",
          "c34000d9c27cc0679bd613b5224a5e10.png",
          "e22514b41d83d13a28c00e1aa3b4293d.png"
        ]
      },
      "aa6f4234ad9f9f846fc2f3c489c30fec": {
        "fr": "TASSE",
        "en": "CUP",
        "images": [
          "19b23ce6c33deb094c1e0cba458d913d.png",
          "1e9243ff24c8bf2489f07339f98019b5.png",
          "3197bd9f3284b9be9d8bd929217b0240.png",
          "caa0afea8af2b6ed9cb5fc7295f448f0.png",
          "e0d9b45a7e985896c5b99bd8fe4f5581.png",
          "e647135fbf67cb93875a1152d74fa29f.png"
        ]
      },
      "8f869cb25ab8bb8f4b17fb94e37aedbc": {
        "fr": "TÉLÉPHONE",
        "en": "TELEPHONE",
        "images": [
          "0b11ded6fd88685efe4e61afbe3ba8b1.png",
          "5631958f4fe9901a67d01a847ddd02a5.png",
          "6601c0ec2b13b0605de0da8bfabba570.png",
          "7a24fe786dd049d2f098c9cccdb64898.png",
          "ac519a57dc91a4b8af3d8340beffb52b.png",
          "cb932a6382e5e1b922f67acc0119e40f.png"
        ]
      },
      "b9ca75a18d6957dc29cafe1c4bb3c573": {
        "fr": "TÉLÉVISION",
        "en": "TELEVISION",
        "images": [
          "069645e6876e7e96821b008c0f7c651e.png",
          "2f9ac2720bf7b06b99d75d679605421d.png",
          "44cbe6252bb5dc37d7ba169add18a5f7.png",
          "4fc6c2b5655da5abfc95816ad2e8fce4.png",
          "cd6aea3bed0fcbf1d89d1d4e0e22d386.png",
          "d091a1fbc516df9dc64bcc44ef11f46c.png"
        ]
      },
      "c2252394e153026e0839c5e25193b0a8": {
        "fr": "THERMOMÈTRE",
        "en": "THERMOMETER",
        "images": [
          "04a57c0b13d2132bd13779b6f860be45.png",
          "4115d94608c05556b66c838b50ad788f.png",
          "6080160af540fc245ce2d0d37db3cc89.png",
          "93fafe16205606ca152a76ca407cc903.png",
          "a67c80d36f6e51915b04f9b86fd42b39.png",
          "f5a94a24e48cdac42892e489affe57af.png"
        ]
      },
      "b2b5a7595880801fb4c3d8e6091a331e": {
        "fr": "TIRE-BOUCHON",
        "en": "CORKSCREW",
        "images": [
          "42f88b87d0ca1fb4d8f1e120ec11df58.png",
          "83a753e4260f7470c80e40525c531791.png",
          "986f50f760d5a1823b4ab7d9599bf7ed.png",
          "c008171f68f86b7f04cd1c9bb955c066.png",
          "c96c37a138fa6fba39843e9b48e4a3bc.png",
          "dc9a326b0a196687ec44f28c30817bbd.png"
        ]
      },
      "43bb2ba3aba0e2a3440002a2db17ecef": {
        "fr": "TIROIR",
        "en": "DRAWER",
        "images": [
          "47a7dc9aac09a8ca679770c39be3922a.png",
          "481f952de63bc22c648259d7e25ed4c4.png",
          "d4d7fbf704b6d3b50df7522ef895420f.png",
          "df14eac44ac816775b8a9007ed0cab75.png",
          "ebcc59cde1b27d1f180b6c7b8ef42e42.png",
          "fd4bd95a52a25b50b98aa7f26f7ea269.png"
        ]
      },
      "d662b766374e0ba5ee26b9885a0b9b86": {
        "fr": "TOBOGGAN",
        "en": "SLIDE",
        "images": [
          "614716aaae9533e4e6c499b64196b0f8.png",
          "66f25bb05c0465396f94dc8691955fa0.png",
          "6ba88113636167cdc8ba5f3a165e145d.png",
          "77c1aeb452902ed8179a6beb03b58c33.png",
          "bd37d3acffbc4656b9daef5648dd7f9b.png",
          "d815d879596cdde7f2423fe98b241654.png"
        ]
      },
      "3f9fc46ae1e2522f069c57fe22d60749": {
        "fr": "TONDEUSE",
        "en": "LAWNMOWER",
        "images": [
          "0fff1e414191cac1cadcbe12f4a88783.png",
          "58c4a59a3ce1ef6db0d91e2d040ae55d.png",
          "5ed79483e1940c836ffa3fe33c927118.png",
          "6e6ff3c7130c89c6cbd1e13f520b1f5b.png",
          "fbfd578fa75a6c24caf0a6e38ac4d62e.png",
          "fec395b33bfb2b4f05d0c35e131fe7de.png"
        ]
      },
      "9119f51aa5a40821c712bf5c3913d577": {
        "fr": "TORCHON",
        "en": "TEA TOWEL",
        "images": [
          "1c849b374f9f5fc8c2ddf21a4f47c6e7.png",
          "1dc04446592dbf05ded1cc54dabe6894.png",
          "4c74683ed3626d8e2b09b6d0d169f6d5.png",
          "4d0d8ae0f768563ccfe951807f7a665f.png",
          "c749e8a029943bd65c49d6f3c37bdb8b.png",
          "f63502fe4955b1866caa4f3e72475dcc.png"
        ]
      },
      "96192153d6abd43387357fdc8821b654": {
        "fr": "TORTUE",
        "en": "TURTLE",
        "images": [
          "12e6863c7762e122f6e6790247739c59.png",
          "299d442275cc5c65022095679517cc33.png",
          "2b3ae0fedb834e7ebfc6c6e9b1b75579.png",
          "430c97dbac58e8120ebe57fa80112b8f.png",
          "7bd856f4965dd773ab8652a2ac0f593f.png",
          "8fd3c595d9ba935b799df32e994c69b2.png"
        ]
      },
      "918eafc6b69c07039ba080078a5676db": {
        "fr": "TOURNEVIS",
        "en": "SCREWDRIVER",
        "images": [
          "17ac50bb48aa50ebff436984d0adf7e5.png",
          "42b88e6b53074b6a1d0c27e3283cd0bd.png",
          "5f90b449a395d46b2f89aa0372e56a35.png",
          "626dd4cca0fbe62f35b9ac930b2dd9a9.png",
          "aafea84611b527ab147cb51a7fc22cb4.png",
          "bb1ef332e63793f639cb62a4b349ef20.png"
        ]
      },
      "f817a49215cc898dbc00f2a72ac81574": {
        "fr": "TRACTEUR",
        "en": "TRACTOR",
        "images": [
          "36fce3daa1c539e4b3be6da85a2a7973.png",
          "4df1cc3775b592b785a6e4f8328648ab.png",
          "6be729ccb88a906f7e87639ecd02fee1.png",
          "90c82530f826edde7e6bfa234f422443.png",
          "a77930743115065d9d6129f6f37661c1.png",
          "e9a3769818c6a478b13a83a9a1a3eb32.png"
        ]
      },
      "42ec57cf6368192e72cc704dc07b373e": {
        "fr": "TRAIN",
        "en": "TRAIN",
        "images": [
          "3057c861c98cfe6920e7a8d896e9a635.png",
          "3e9afdfeb2eb753bcc93df34dc03871b.png",
          "6d146be56fc29cf959dc1f63b54b3ff8.png",
          "6fcd732b3f3cafe789be179198b4baae.png",
          "aebd51f5f8d8e229cce7e8350d06e0cc.png",
          "dd071e980a5c90b9fedb833a76cb1153.png"
        ]
      },
      "7ec787878d133e00ba151ffc0ff58228": {
        "fr": "TROMPETTE",
        "en": "TRUMPET",
        "images": [
          "1c87ccdf5acd00893051909884718eeb.png",
          "309ab6622987a0842b2048e6e2cf7744.png",
          "33ca0ef56bf13c4a48d005e4aab7875a.png",
          "7055c72195abc1aedffc058ec51f990e.png",
          "cfe5ffc3927f95adae259a1abf375e40.png",
          "e873b66c4b17506063bd4abffa7e1d68.png"
        ]
      },
      "1333c6efcc1e0f50b9d9d2e770226341": {
        "fr": "TULIPE",
        "en": "TULIP",
        "images": [
          "1cdfb34bc1cbb553e04dbecf392d8fe4.png",
          "37b003730775034e1dd0b444813651c0.png",
          "88c60d7313c8a07527ba385fb7c86c25.png",
          "98fa020490241c02db5719ebef0ee38e.png",
          "b87afdda428fb2a4df88d73b6e2b03e5.png",
          "f60ed7474b470383159513ca2aed8e48.png"
        ]
      },
      "4d3377476fc735237fda1a29ab6f7f4e": {
        "fr": "VACHE",
        "en": "COW",
        "images": [
          "07cb93a33926f5213a3f063c729c2b43.png",
          "6175273e4ad847aa07c557f3c7ba1f93.png",
          "61ae7bf81e76a282c73b36883fcd2de1.png",
          "6255a137c17b7e7fa972d64d32c5df1f.png",
          "72c1b418f8e27c2696c19dd8a158ca7b.png",
          "73be08658ea072a3287ed6183482b51e.png"
        ]
      },
      "06ef54d9554760675267b15d605a6ed8": {
        "fr": "VALISE",
        "en": "SUITCASE",
        "images": [
          "04c873d647e8afc2815a0e365db65650.png",
          "0b51260d994aedee064139d9a43c2616.png",
          "60504196e52cf9033012ea846d810ed7.png",
          "97070e789497e327429ac4c0ec7b5e93.png",
          "e0204ef91baf3d015daf86950ad97983.png",
          "fd317db50a0cf4a28b1104050b934ac2.png"
        ]
      },
      "216283d5fc365a709d8a03e965e2797c": {
        "fr": "VASE",
        "en": "VASE",
        "images": [
          "17e620fd71ff6005a7baed0f246a6772.png",
          "588c1be6f4f11a9dec1a9ae3d7571e1b.png",
          "603a0e4a171f477a6f8ed1065ecd3708.png",
          "8d8d755f03449691cd59a9990bcc5a40.png",
          "a20f431c77b64450ad941b6a31a7d2c6.png",
          "a38a3831c07f99458c5edd98e7b27ded.png"
        ]
      },
      "bd3e82d42bcb06ce32e1cb2208287b7b": {
        "fr": "VÉLO",
        "en": "BIKE",
        "images": [
          "001de6d5a302d5a058187e8232394fe8.png",
          "180d849b720bf5bf010c4608b6819406.png",
          "3a31929dd7f1d15526f2a8b30968c1dc.png",
          "535dd979fe84e3eb12dc38956400b375.png",
          "85ed97888bc57f18b34e206edefaf5bb.png",
          "cc8b4d817bafee77717c39b9601ae701.png"
        ]
      },
      "0b5817fc6bff8c439ca85fa2b4eb201b": {
        "fr": "VIOLON",
        "en": "VIOLIN",
        "images": [
          "020fab85099a3b750a69748527fb747a.png",
          "2e604d5bcdf6a9e40cebdfaa4f12ed38.png",
          "5ebc17e48b6833bd5c9850d0ac6b62a6.png",
          "cd62cc4a0be559c2b1ad63997d5f5d6d.png",
          "ce1ab6881f545ddd09a6d79d2550853d.png",
          "d9f88d4ade1fe3971ad3db26e56708f2.png"
        ]
      },
      "e7ff5e99c22ea3320dd40aedd9dd59d3": {
        "fr": "VOILIER",
        "en": "SAILBOAT",
        "images": [
          "0297ea4bbd57b6725ba38bf5c3c62b15.png",
          "36bf103a4310704a80a6eb6f09d6f7ff.png",
          "647c1ff448b9d901eeaa0d61055d605c.png",
          "833773b0c2a41093964f5c9734a59097.png",
          "8722336a0ea1b933d9b85d8af572c594.png",
          "b5df827de809417b19699a58669ac0ad.png"
        ]
      },
      "0facd56e3194ee0e9b30fa56054f445d": {
        "fr": "VOITURE",
        "en": "CAR",
        "images": [
          "18ead3ab1c9cdeb55643508fbb1f6180.png",
          "6e65aee9e09d93036503b20a48301958.png",
          "8c6562451e05511e6ce04610508a2510.png",
          "95c69094d14291a987eeb1dc0f4871c6.png",
          "f41f431bac2a67199a228956fa084d61.png",
          "fba33f72f958bc5d1b43bb5a6c2990c3.png"
        ]
      },
      "ada58105f52eb239b7affda0478f8b12": {
        "fr": "YAOURT",
        "en": "YOGURT",
        "images": [
          "0dac21c9fd5e6a5dc8fb4467880cde63.png",
          "17ee3f71c33b7f093eea96b2828ed1c1.png",
          "2be5abd98f2412cbce407cab29fce944.png",
          "2e3b3191abd3193d25057e092bfeb147.png",
          "a3457fb2af33e83705c7caa8484ddea9.png",
          "e039ac53e75b6b16e0d46113e3cb53e1.png"
        ]
      },
      "6cbb159a3d46051815ac39714e6337c5": {
        "fr": "ZÈBRE",
        "en": "ZEBRA",
        "images": [
          "6684de3f0a52a9064d41a634d34cf956.png",
          "876073024d949a87d62f775ab6f77c68.png",
          "9a760bebf5f83dea1ebf0abb53658247.png",
          "9ad62a6182cfbbe7e649d2bbafb06396.png",
          "a169802bf85ba282a94f5a3454aa6197.png",
          "bb6481e116333cc7d7fedfa96c0ef564.png"
        ]
      }
    }
  };
}
