import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/data/game_data.dart';
import 'package:wordguessing/models/data/word.dart';

class FetchDataHelper {
  FetchDataHelper();

  List<Word> _words = [];
  String _lang = '';

  void init(String lang) {
    _words = [];
    _lang = lang;

    try {
      const gameData = GameData.data;

      final Map<String, dynamic> rawWords = gameData['words'] as Map<String, dynamic>;
      rawWords.forEach((String key, rawWord) {
        final String text = rawWord[_lang] as String;
        final List<String> images = [];
        for (var rawImage in rawWord['images'] as List<dynamic>) {
          images.add(rawImage.toString());
        }

        images.shuffle();

        if (images.isNotEmpty) {
          _words.add(Word(
            key: key,
            text: text,
            images: images,
          ));
        }
      });
    } catch (e) {
      printlog("$e");
    }
  }

  List<Word> getWords({required String lang, required int count}) {
    if (_words.isEmpty || lang != _lang) {
      init(lang);
    }

    List<Word> words = _words;

    words.shuffle();

    return words.take(count).toList();
  }
}
