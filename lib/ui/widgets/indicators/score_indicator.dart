import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

class ScoreIndicator extends StatelessWidget {
  const ScoreIndicator({
    super.key,
    required this.text,
    required this.blockColor,
  });

  final String text;
  final Color blockColor;

  @override
  Widget build(BuildContext context) {
    final Color borderColor = blockColor.darken();

    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 15),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: blockColor,
        borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: borderColor,
          width: 4,
        ),
      ),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.w600,
          color: Colors.black,
        ),
      ),
    );
  }
}
