import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/config/application_config.dart';
import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/data/word.dart';
import 'package:wordguessing/models/activity/activity.dart';
import 'package:wordguessing/ui/widgets/game/pick_image_game_image.dart';

class PickImageGameItemsContainer extends StatelessWidget {
  const PickImageGameItemsContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        final List<Word> images = currentActivity.images;

        if (images.length != ApplicationConfig.itemsCount) {
          return const SizedBox.shrink();
        }

        images.sort((a, b) => a.key.compareTo(b.key));

        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickImageGameImageContainer(word: images[0]),
                PickImageGameImageContainer(word: images[1]),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickImageGameImageContainer(word: images[2]),
                PickImageGameImageContainer(word: images[3]),
              ],
            )
          ],
        );
      },
    );
  }
}
