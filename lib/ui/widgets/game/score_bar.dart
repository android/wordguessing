import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/activity/activity.dart';
import 'package:wordguessing/ui/widgets/indicators/score_indicator.dart';

class ScoreBarWidget extends StatelessWidget {
  const ScoreBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final int count = currentActivity.questionsCount - 1;

        final String goodAnswersString = '👍 ${currentActivity.goodAnswers}/$count';
        final String wrongAnswersString = '🚩 ${currentActivity.wrongAnswers}/$count';

        return Container(
          padding: const EdgeInsets.all(5),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ScoreIndicator(
                text: goodAnswersString,
                blockColor: Colors.green,
              ),
              const SizedBox(width: 20),
              ScoreIndicator(
                text: wrongAnswersString,
                blockColor: Colors.orange,
              ),
            ],
          ),
        );
      },
    );
  }
}
