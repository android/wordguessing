import 'package:flutter/material.dart';

import 'package:wordguessing/ui/widgets/game/pick_word_game_image_items.dart';
import 'package:wordguessing/ui/widgets/game/pick_word_game_word_items.dart';

class GamePickWordPage extends StatelessWidget {
  const GamePickWordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        PickWordGameImageItemsContainer(),
        SizedBox(height: 8),
        PickWordGameWordItemsContainer(),
      ],
    );
  }
}
