import 'package:flutter/material.dart';

import 'package:wordguessing/ui/widgets/game/pick_image_game_items.dart';
import 'package:wordguessing/ui/widgets/game/pick_image_game_word.dart';

class GamePickImagePage extends StatelessWidget {
  const GamePickImagePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        PickImageGameWordContainer(),
        SizedBox(height: 8),
        PickImageGameItemsContainer(),
      ],
    );
  }
}
