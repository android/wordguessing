import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/data/word.dart';

class PickWordGameWordContainer extends StatelessWidget {
  const PickWordGameWordContainer({super.key, required this.word});

  final Word word;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        color: Colors.blue[800],
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
          color: Colors.white,
          width: 6,
        ),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(15),
        ),
        child: Text(
          word.text,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
        ),
        onPressed: () {
          BlocProvider.of<ActivityCubit>(context).checkWord(word);
        },
      ),
    );
  }
}
