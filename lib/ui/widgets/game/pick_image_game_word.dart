import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/activity/activity.dart';

class PickImageGameWordContainer extends StatelessWidget {
  const PickImageGameWordContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Container(
          decoration: BoxDecoration(
            color: Colors.blue.shade800,
            borderRadius: BorderRadius.circular(6),
            border: Border.all(
              color: Colors.white,
              width: 6,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Text(
              currentActivity.word.text,
              style: const TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
          ),
        );
      },
    );
  }
}
