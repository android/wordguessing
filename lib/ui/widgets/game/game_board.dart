import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/config/application_config.dart';
import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/activity/activity.dart';
import 'package:wordguessing/ui/widgets/game/game_pick_image.dart';
import 'package:wordguessing/ui/widgets/game/game_pick_word.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: BlocBuilder<ActivityCubit, ActivityState>(
        builder: (BuildContext context, ActivityState activityState) {
          final Activity currentActivity = activityState.currentActivity;

          switch (
              currentActivity.activitySettings.get(ApplicationConfig.parameterCodeGameType)) {
            case ApplicationConfig.gameTypeValuePickImage:
              return const GamePickImagePage();
            case ApplicationConfig.gameTypeValuePickWord:
              return const GamePickWordPage();
            default:
              return const SizedBox.shrink();
          }
        },
      ),
    );
  }
}
