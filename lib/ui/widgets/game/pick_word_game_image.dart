import 'package:flutter/material.dart';

class PickWordGameImageContainer extends StatelessWidget {
  const PickWordGameImageContainer({super.key, required this.image});

  final String image;

  @override
  Widget build(BuildContext context) {
    const double imageSize = 130;

    String imageAsset = 'assets/ui/placeholder.png';
    if (image != '') {
      imageAsset = 'assets/images/$image';
    }

    return Container(
      margin: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Colors.blue.shade200,
          width: 8,
        ),
      ),
      child: Image(
        image: AssetImage(imageAsset),
        width: imageSize,
        height: imageSize,
        fit: BoxFit.fill,
      ),
    );
  }
}
