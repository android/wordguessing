import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/data/word.dart';
import 'package:wordguessing/models/activity/activity.dart';
import 'package:wordguessing/ui/widgets/game/pick_word_game_image.dart';

class PickWordGameImageItemsContainer extends StatelessWidget {
  const PickWordGameImageItemsContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        if (currentActivity.images.isEmpty) {
          return const SizedBox.shrink();
        }

        final Word word = currentActivity.images[0];

        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickWordGameImageContainer(image: word.images[0]),
                PickWordGameImageContainer(image: word.images[1]),
              ],
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickWordGameImageContainer(image: word.images[2]),
                PickWordGameImageContainer(image: word.images[3]),
              ],
            )
          ],
        );
      },
    );
  }
}
