import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/data/word.dart';

class PickImageGameImageContainer extends StatelessWidget {
  const PickImageGameImageContainer({super.key, required this.word});

  final Word word;

  @override
  Widget build(BuildContext context) {
    const double imageSize = 130;

    String imageAsset = 'assets/ui/placeholder.png';
    if ((word.images.isNotEmpty)) {
      imageAsset = 'assets/images/${word.images[0]}';
    }

    return TextButton(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: Colors.blue.shade200,
            width: 8,
          ),
        ),
        margin: const EdgeInsets.all(2),
        child: Image(
          image: AssetImage(imageAsset),
          width: imageSize,
          height: imageSize,
          fit: BoxFit.fill,
        ),
      ),
      onPressed: () {
        BlocProvider.of<ActivityCubit>(context).checkWord(word);
      },
    );
  }
}
