import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/config/application_config.dart';
import 'package:wordguessing/cubit/activity/activity_cubit.dart';
import 'package:wordguessing/models/data/word.dart';
import 'package:wordguessing/models/activity/activity.dart';
import 'package:wordguessing/ui/widgets/game/pick_word_game_word.dart';

class PickWordGameWordItemsContainer extends StatelessWidget {
  const PickWordGameWordItemsContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        Word? word = currentActivity.word;
        List<Word> otherWords = currentActivity.otherWords;

        if (otherWords.length != (ApplicationConfig.itemsCount - 1)) {
          return const Column();
        }

        List<Word> words = [
          word,
          otherWords[0],
          otherWords[1],
          otherWords[2],
        ];

        words.sort((a, b) => a.key.compareTo(b.key));

        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickWordGameWordContainer(word: words[0]),
                const SizedBox(width: 10),
                PickWordGameWordContainer(word: words[1]),
              ],
            ),
            const SizedBox(height: 5),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PickWordGameWordContainer(word: words[2]),
                const SizedBox(width: 10),
                PickWordGameWordContainer(word: words[3]),
              ],
            )
          ],
        );
      },
    );
  }
}
