import 'package:flutter/material.dart';

import 'package:wordguessing/ui/widgets/game/score_bar.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const ScoreBarWidget();
  }
}
