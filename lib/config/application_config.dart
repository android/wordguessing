import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:wordguessing/cubit/activity/activity_cubit.dart';

import 'package:wordguessing/ui/pages/game.dart';

class ApplicationConfig {
  // activity parameter: game type
  static const String parameterCodeGameType = 'type';
  static const String gameTypeValuePickWord = 'pick-word';
  static const String gameTypeValuePickImage = 'pick-image';

  // activity parameter: language
  static const String parameterCodeLangValue = 'lang';
  static const String langValueFr = 'fr';
  static const String langValueEn = 'en';

  static const int itemsCount = 4;
  static const int recentWordsCount = 20;

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Words Game',
    activitySettings: [
      // game type
      ApplicationSettingsParameter(
        code: parameterCodeGameType,
        values: [
          ApplicationSettingsParameterItemValue(
            value: gameTypeValuePickWord,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: gameTypeValuePickImage,
          ),
        ],
        builder: ({
          required context,
          required itemValue,
          required onPressed,
          required size,
        }) =>
            StyledButton(
          color: Colors.teal,
          onPressed: onPressed,
          child: Image(
            image: AssetImage('assets/ui/type_${itemValue.value}.png'),
            fit: BoxFit.fill,
          ),
        ),
      ),

      // lang
      ApplicationSettingsParameter(
        code: parameterCodeLangValue,
        values: [
          ApplicationSettingsParameterItemValue(
            value: langValueFr,
            color: Colors.teal,
            text: '🇫🇷',
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: langValueEn,
            color: Colors.teal,
            text: '🇬🇧',
          ),
        ],
        builder: ({
          required context,
          required itemValue,
          required onPressed,
          required size,
        }) =>
            StyledButton(
          color: Colors.teal,
          onPressed: onPressed,
          child: Text(
            itemValue.text ?? '',
            style: TextStyle(
              fontSize: 50,
            ),
          ),
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
