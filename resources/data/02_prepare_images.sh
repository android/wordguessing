#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

IMAGES_CACHE_FOLDER="${CURRENT_DIR}/cache"

IMAGES_DOWNLOAD_FOLDER="${IMAGES_CACHE_FOLDER}/download"
IMAGES_SELECTED_FOLDER="${IMAGES_CACHE_FOLDER}/selected"

# delete empty/temp files
find "${IMAGES_CACHE_FOLDER}" -type f -name "*.png" -empty -exec rm {} \;
find "${IMAGES_CACHE_FOLDER}" -type f -name "*.tmp*" -exec rm {} \;
find "${IMAGES_CACHE_FOLDER}" -type d -empty -exec rm -rf {} \;

# copy missing files from "download" to "selected"
rsync --ignore-existing -raz --progress ${IMAGES_DOWNLOAD_FOLDER}/* "${IMAGES_SELECTED_FOLDER}"

echo "done: copy files from download to selected."
echo "please remove unwanted files from selected folders."
