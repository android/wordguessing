#!/usr/bin/env bash

command -v jq >/dev/null 2>&1 || {
  echo >&2 "I require jq (json parser) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "${CURRENT_DIR}")"

IMAGES_CACHE_FOLDER="${CURRENT_DIR}/cache"
IMAGES_OPTIMIZED_FOLDER="${IMAGES_CACHE_FOLDER}/optimized"
OUTPUT_IMAGES_BOARD_FILE="${IMAGES_CACHE_FOLDER}/images_board.html"
touch "${OUTPUT_IMAGES_BOARD_FILE}"

echo "Cleaning empty/temp files..."
find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" -empty -exec rm {} \;
find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.tmp*" -exec rm {} \;
find "${IMAGES_OPTIMIZED_FOLDER}" -type d -empty -exec rm -rf {} \;

echo "Building images board file..."
FILES="$(find "${IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" | sed "s|^${IMAGES_OPTIMIZED_FOLDER}/||g" | sort)"
KEYWORDS="$(echo "${FILES}" | cut -d'/' -f1 | sort | uniq)"

echo "<!DOCTYPE html>" >"${OUTPUT_IMAGES_BOARD_FILE}"
echo "<html lang=\"en\">" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "  <head>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "    <style>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "      table { border-collapse: collapse; border: solid 2px black; width: 100%; }" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "      tr { }" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "      td { border: solid 1px black; }" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "      span { font-weight: bold; }" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "      img { border: solid 1px grey; }" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "    </style>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "  </head>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "  <body>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "    <table>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
while read -r KEYWORD; do
  if [[ -n "${KEYWORD}" ]]; then
    echo "      <tr>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
    echo "        <td><span>${KEYWORD}</span></td>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
    FILES_KEYWORD="$(echo "${FILES}" | grep -e "^${KEYWORD}/")"
    echo "        <td>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
    while read -r FILE; do
      echo "          <img src=\"optimized/${FILE}\">" >>"${OUTPUT_IMAGES_BOARD_FILE}"
    done < <(echo "${FILES_KEYWORD}")
    echo "        </td>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
    echo "      </tr>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
  fi
done < <(echo "${KEYWORDS}")
echo "    </table>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "  </body>" >>"${OUTPUT_IMAGES_BOARD_FILE}"
echo "</html>" >>"${OUTPUT_IMAGES_BOARD_FILE}"

echo "done."
