#!/usr/bin/env bash

command -v jq >/dev/null 2>&1 || {
  echo >&2 "I require jq (json parser) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "${CURRENT_DIR}")"

# CSV source file for words
SOURCE_CSV_FILE="${CURRENT_DIR}/words.csv"

ASSETS_BASE_FOLDER="${BASE_DIR}/assets"

OUTPUT_WORDS_LIST="${CURRENT_DIR}/words.json"
touch "${OUTPUT_WORDS_LIST}"

TARGET_IMAGES_ASSETS_FOLDER="${ASSETS_BASE_FOLDER}/images"

IMAGES_CACHE_FOLDER="${CURRENT_DIR}/cache"
SOURCE_IMAGES_OPTIMIZED_FOLDER="${IMAGES_CACHE_FOLDER}/optimized"

echo "Cleaning empty/temp files..."
find "${SOURCE_IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" -empty -exec rm {} \;
find "${SOURCE_IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.tmp*" -exec rm {} \;
find "${SOURCE_IMAGES_OPTIMIZED_FOLDER}" -type d -empty -exec rm -rf {} \;

echo "Cleaning existing assets..."
find "${TARGET_IMAGES_ASSETS_FOLDER}" -type f -name "*.png" -exec rm {} \;

echo "Building words/images assets json file..."

# Existing images in optimized folder
FILES="$(find "${SOURCE_IMAGES_OPTIMIZED_FOLDER}" -type f -name "*.png" | sed "s|^${SOURCE_IMAGES_OPTIMIZED_FOLDER}/||g" | sort)"

OUTPUT_WORDS_LIST_TMP="${OUTPUT_WORDS_LIST}.tmp"
echo "{" >"${OUTPUT_WORDS_LIST_TMP}"
echo "  \"words\": {" >>"${OUTPUT_WORDS_LIST_TMP}"
FIRST_WORD=1
while read -r LINE; do
  if [[ -n "${LINE}" ]]; then
    echo "- ${LINE}"

    WORD_FR="$(echo "${LINE}" | cut -d';' -f1)"
    WORD_EN="$(echo "${LINE}" | cut -d';' -f2)"
    KEY="$(echo "${LINE}" | md5sum | awk '{print $1;}')"

    if [[ ${FIRST_WORD} -eq 0 ]]; then
      echo "    ," >>"${OUTPUT_WORDS_LIST_TMP}"
    fi

    echo "    \"${KEY}\": {" >>"${OUTPUT_WORDS_LIST_TMP}"
    echo "      \"fr\": \"${WORD_FR}\"," >>"${OUTPUT_WORDS_LIST_TMP}"
    echo "      \"en\": \"${WORD_EN}\"," >>"${OUTPUT_WORDS_LIST_TMP}"

    echo "      \"images\": [" >>"${OUTPUT_WORDS_LIST_TMP}"

    FOLDER_KEY="${WORD_FR}"
    FILES_FOR_THIS_WORD="$(echo "${FILES}" | grep -E "^${FOLDER_KEY}/")"
    if [[ -n "${FILES_FOR_THIS_WORD}" ]]; then
      FIRST_FILE=1
      while read -r FILE; do
        if [[ ${FIRST_FILE} -eq 0 ]]; then
          echo "      ," >>"${OUTPUT_WORDS_LIST_TMP}"
        fi
        echo "      \"$(echo "${FILE}" | sed "s|${FOLDER_KEY}/||g")\"" >>"${OUTPUT_WORDS_LIST_TMP}"
        FIRST_FILE=0
        cp -f "${SOURCE_IMAGES_OPTIMIZED_FOLDER}/${FILE}" "${TARGET_IMAGES_ASSETS_FOLDER}"
      done < <(echo "${FILES_FOR_THIS_WORD}")
    else
      echo "No image found for ${KEY} / ${FOLDER_KEY}"
    fi

    echo "      ]" >>"${OUTPUT_WORDS_LIST_TMP}"
    echo "    }" >>"${OUTPUT_WORDS_LIST_TMP}"

    FIRST_WORD=0
  fi
done < <(cat "${SOURCE_CSV_FILE}" | sort | uniq)

echo "  }" >>"${OUTPUT_WORDS_LIST_TMP}"
echo "}" >>"${OUTPUT_WORDS_LIST_TMP}"

# Format json
cat "${OUTPUT_WORDS_LIST_TMP}" | jq >"${OUTPUT_WORDS_LIST}"
rm "${OUTPUT_WORDS_LIST_TMP}"

# inject json file in app code
GAME_DATA_DART_FILE="${BASE_DIR}/lib/data/game_data.dart"
echo "class GameData {" >"${GAME_DATA_DART_FILE}"
echo "  static const Map<String, dynamic> data = $(cat "${OUTPUT_WORDS_LIST}");" >>"${GAME_DATA_DART_FILE}"
echo "}" >>"${GAME_DATA_DART_FILE}"

dart format "${GAME_DATA_DART_FILE}"

echo "done."
